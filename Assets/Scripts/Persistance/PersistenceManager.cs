﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class PersistenceManager : PersistentSingleton<PersistenceManager>, IRequireSetup
{
    [SerializeField] private string storagePath = @"settings.data";
    
    private Dictionary<string, Data> storage = new Dictionary<string, Data>();
    
    public void Setup()
    {
        LoadData();
    }

    #region Public access
    public static void UpdateData<T>(T data) where T : Data
    {
        Instance.UpdateDataInternal(data);
    }

    public static T GetData<T>(string id) where T : Data
    {
        return Instance.GetDataInternal<T>(id);
    }

    public static void LoadData()
    {
        Instance.LoadDataInternal();
    }

    public static void SaveData()
    {
        Instance.SaveDataInternal();
    }
    #endregion

    #region Private logic
    public void UpdateDataInternal<T>(T data) where T : Data
    {
        if (!storage.ContainsKey(data.id))
        {
            storage.Add(data.id, data);
            return;
        }

        storage[data.id] = data;
    }

    public T GetDataInternal<T>(string id) where T : Data
    {
        return storage.ContainsKey(id) ? (T)storage[id] : null;
    }

    public void LoadDataInternal()
    {
        Instance.ReadFromFile(storagePath);
    }

    public void SaveDataInternal()
    {
        Instance.WriteToFile(storagePath);
    }
    #endregion

    #region File read/write
    private bool ReadFromFile(string path)
    {
        if (!File.Exists(path))
            return false;

        storage.Clear();

        using (Stream stream = File.Open(path, FileMode.Open))
        {
            var binaryFormatter = new BinaryFormatter();
            storage = (Dictionary<string, Data>)binaryFormatter.Deserialize(stream);
        }
        return true;
    }

    private bool WriteToFile(string path)
    {
        using (Stream stream = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
        {
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, storage);
        }
        return true;
    }
    #endregion
}
