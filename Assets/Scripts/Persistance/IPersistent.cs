﻿using System;
using UnityEngine;

public interface IPersistent
{
    void LoadData(Data data);
}

[Serializable]
public abstract class Data : IPersistent
{
    public string id;

    public abstract void LoadData(Data data);
}

[Serializable]
public class Data<T> : Data
{
    [SerializeField] protected T value;

    public Data(string id, T value)
    {
        this.id = id;
        this.value = value;
    }

    public override void LoadData(Data data)
    {
        Data<T> local = (Data<T>)data;
        id = local.id;
        value = local.value;
    }
}

[Serializable]
public class Data<T0, T1> : Data
{
    [SerializeField] protected T0 value0;
    [SerializeField] protected T1 value1;

    public Data(string id, T0 value0, T1 value1)
    {
        this.id = id;
        this.value0 = value0;
        this.value1 = value1;
    }

    public override void LoadData(Data data)
    {
        Data<T0, T1> local = (Data<T0, T1>)data;

        id = local.id;
        value0 = local.value0;
        value1 = local.value1;
    }
}

[Serializable]
public class Data<T0, T1, T2> : Data
{
    [SerializeField] protected T0 value0;
    [SerializeField] protected T1 value1;
    [SerializeField] protected T2 value2;

    public Data(string id, T0 value0, T1 value1, T2 value2)
    {
        this.id = id;
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
    }

    public override void LoadData(Data data)
    {
        Data<T0, T1, T2> local = (Data<T0, T1, T2>)data;

        id = local.id;
        value0 = local.value0;
        value1 = local.value1;
        value2 = local.value2;
    }
}

[Serializable]
public class Data<T0, T1, T2, T3> : Data
{
    [SerializeField] protected T0 value0;
    [SerializeField] protected T1 value1;
    [SerializeField] protected T2 value2;
    [SerializeField] protected T3 value3;

    public Data(string id, T0 value0, T1 value1, T2 value2, T3 value3)
    {
        this.id = id;
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }

    public override void LoadData(Data data)
    {
        Data<T0, T1, T2, T3> local = (Data<T0, T1, T2, T3>)data;

        id = local.id;
        value0 = local.value0;
        value1 = local.value1;
        value2 = local.value2;
        value3 = local.value3;
    }
}

[Serializable]
public class Data<T0, T1, T2, T3, T4> : Data
{
    [SerializeField] protected T0 value0;
    [SerializeField] protected T1 value1;
    [SerializeField] protected T2 value2;
    [SerializeField] protected T3 value3;
    [SerializeField] protected T4 value4;

    public Data(string id, T0 value0, T1 value1, T2 value2, T3 value3, T4 value4)
    {
        this.id = id;
        this.value0 = value0;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public override void LoadData(Data data)
    {
        Data<T0, T1, T2, T3, T4> local = (Data<T0, T1, T2, T3, T4>)data;

        id = local.id;
        value0 = local.value0;
        value1 = local.value1;
        value2 = local.value2;
        value3 = local.value3;
        value4 = local.value4;
    }
}