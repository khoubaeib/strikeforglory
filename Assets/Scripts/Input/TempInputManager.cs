﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class TempInputManager : PersistentSingleton<TempInputManager>
{
    public Action<int> OnPlayerJoin;
    public Action<int> OnPlayerLeave;

    [SerializeField] private GamepadConfig defaultConfig;
    [SerializeField] private float refreshTime = 1f;

    private List<GamepadWrapper> gamepads = new List<GamepadWrapper>();

    private float lastRefreshTime = 0;
    
    public int GamePadsCount
    {
        get { return gamepads.Count; }
    }

    public List<GamepadWrapper> GamePads
    {
        get { return gamepads; }
    }

    void Start()
    {
        RefreshGamepadsList();
    }

    void Update()
    {
        CheckGamepads();
        SendInput();

        if (Input.GetKeyDown(KeyCode.Escape))
            MatchManager.Instance.PauseGame();
    }

    private void CheckGamepads()
    {
        if (lastRefreshTime > Time.time)
            return;
        
        RefreshGamepadsList();
        lastRefreshTime = Time.time + refreshTime;
    }

    public void RefreshGamepadsList()
    {
        if (!GamepadsListChanged())
            return;

        
        for (int i = gamepads.Count; i < Input.GetJoystickNames().Length; i++)
            ConnectGamepad();

        for (int i = Input.GetJoystickNames().Length; i < gamepads.Count; i++)
            DisconnectGamepad(i); 
    }

    private bool GamepadsListChanged()
    {

        return Input.GetJoystickNames().Length != GamePadsCount;
        /*PlayerIndex singlePlayer = (PlayerIndex)0;
        GamePadState testState = GamePad.GetState(singlePlayer);

        if (testState.IsConnected)
        {
            //joystick is connected
        }
        else
        {
            //joystick is not connected
        }*/

        //return false;

        /*if(Input.GetJoystickNames().)

        foreach (var j in Input.GetJoystickNames())
        {
            pads = pads + (j.Equals("") ? 0 : 1);
        }

        return pads == gamepads.Count;
        */
    }


    private void ConnectGamepad()
    {
        /*gamepads.Add(new GamepadWrapper(gamepads.Count + 1));
        
        if (OnPlayerJoin != null)
            OnPlayerJoin(gamepads.Count);*/
    }

    private void DisconnectGamepad(int index)
    {
        /*gamepads[index] = null;

        if (OnPlayerLeave != null)
            OnPlayerLeave(index);*/
    }
    
    private void SendInput()
    {
        for(int i = 0 ; i < gamepads.Count; i++)
        {
            if (gamepads[i] == null)
                continue;

            //GameManager.Instance.ReceiveInput(gamepads[i]);
        }
    }
}
