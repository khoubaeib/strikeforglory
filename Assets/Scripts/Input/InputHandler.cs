﻿using Rewired;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public enum InputCategory : int
{
    Default = 0,
    UI = 1
}

public class InputHandler : PersistentSingleton<InputHandler>, IRequireSetup
{
    public Action<int> OnConnectGamepad;
    public Action<int> OnDisconnectGamepad;

    public Action<int> OnPlayerJoin;
    public Action<int> OnPlayerLeave;

    private Dictionary<int, GamepadWrapper> gamepads = new Dictionary<int, GamepadWrapper>();
    public Dictionary<int, GamepadWrapper> Gamepads
    {
        get { return gamepads; }
    }
    
    public int ConnectedPlayersCount
    {
        get { return gamepads.Values.Count(gp => gp.IsConnected); }
    }

    private InputCategory category;
    public InputCategory Category
    {
        get { return category; }
        set
        {
            category = value;
            foreach (GamepadWrapper  gp in gamepads.Values)
            {
                if (gp.IsConnected)
                    continue;
                
                var joysticks = ReInput.players.GetPlayer(gp.Id).controllers.Joysticks;
                string c = Enum.GetName(typeof(InputCategory), category);
                foreach (var j in joysticks)
                {
                    ReInput.players.GetPlayer(gp.Id).controllers.maps.LoadMap(ControllerType.Joystick, j.id, c, "Default");
                }
            }
        }
    }
    
    public void Setup()
    {
        InitControllers();

        Category = InputCategory.UI;
        Cursor.visible = false;

        ReInput.ControllerConnectedEvent += OnGamepadConnected;
        ReInput.ControllerDisconnectedEvent += OnGamepadDisconnected;
    }

    void Update()
    {
        CheckZombiController();
        SendInput();
    }

    #region Controllers Connection/Disconnection handling
    private void InitControllers()
    {
        gamepads = new Dictionary<int, GamepadWrapper>();
        foreach (var p in ReInput.players.Players)
        {
            bool present = p.controllers.joystickCount > 0;
            gamepads.Add(p.id, new GamepadWrapper(p.id, present, present));
        }

        GameData.Instance.PlayersCount = ConnectedPlayersCount;
    }
    
    private void OnGamepadConnected(ControllerStatusChangedEventArgs eventArgs)
    {
        int playerID = GetPlayerID(eventArgs.controllerId);
        if (!gamepads.ContainsKey(playerID))
            return;
         
        gamepads[playerID].IsConnected = true;

        bool inGame = MatchManager.Instance;
        gamepads[playerID].IsPlaying = !inGame;
        
        if (OnConnectGamepad != null)
            OnConnectGamepad(eventArgs.controllerId);

        if (!inGame && OnPlayerJoin != null)
        {
            GameData.Instance.PlayersCount = ConnectedPlayersCount;
            OnPlayerJoin(eventArgs.controllerId);
        }
    }

    private void OnGamepadDisconnected(ControllerStatusChangedEventArgs eventArgs)
    {
        int playerID = eventArgs.controllerId;
        
        gamepads[playerID].IsConnected = false;
        gamepads[playerID].IsPlaying = false;

        GameData.Instance.PlayersCount = ConnectedPlayersCount;

        if (OnDisconnectGamepad != null)
            OnDisconnectGamepad(playerID);
        
        if (OnPlayerLeave != null)
            OnPlayerLeave(playerID);
    }

    private int GetPlayerID(int controllerId)
    {
        foreach (var p in ReInput.players.Players)
        {
            if (p.controllers.ContainsController<Joystick>(controllerId))
                return p.id;
        }
        
        return -1;
    }
    #endregion

    #region Controller customisation
    public void UpdateGamepadButton(int gamepad, GamepadButton button, CharacterAction action)
    {
        //gamepads[gamepad].UpdateButton(button, action);
    }

    public void SetActiveLeftStick(int gamepad, bool active)
    {
        gamepads[gamepad].UseLeftStick = active;
    }

    public void SetActiveRightStick(int gamepad, bool active)
    {
        gamepads[gamepad].UseRightStick = active;
    }

    public void SetActiveDPad(int gamepad, bool active)
    {
        gamepads[gamepad].UseDPad = active;
    }
    #endregion

    #region Input Processing
    private void CheckZombiController()
    {
        foreach (var gp in gamepads.Values)
        {
            if (!gp.IsConnected || gp.IsPlaying)
                continue;

            bool join = gp.CheckJoinRequest();
            if (!join)
                continue;

            gp.IsPlaying = true;
            GameData.Instance.PlayersCount = ConnectedPlayersCount;

            if (OnPlayerJoin != null) 
                OnPlayerJoin(gp.Id);
        }
    }

    private void SendInput()
    {
        if (gamepads.Count == 0 || !MatchManager.Instance)
            return;

        foreach (var gp in gamepads.Values)
        {
            if (!gp.IsConnected || !gp.IsPlaying)
                continue;

            if (gp.GetMenu())
            {
                MatchManager.Instance.DoPauseResumeCycle();
                return;
            }

            MatchManager.Instance.ReceiveCharacterInput(gp);
        }
    }
    #endregion
}
