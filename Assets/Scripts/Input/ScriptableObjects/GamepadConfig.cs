﻿using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class InputEntry
{
    public KeyCode Primary
    {
        get { return primary; }
    }

    public KeyCode Secondary
    {
        get { return secondary; }
    }

    [SerializeField] private KeyCode primary;
    [SerializeField] private KeyCode secondary;

    public InputEntry(string primary, string secondary) { }

    public InputEntry(KeyCode primary, KeyCode secondary) { }

    public bool IsDown
    {
        get { return false; }
    }

    public bool IsUp
    {
        get { return false; }
    }
}

public class GamepadConfig : ScriptableObject, ISerializationCallbackReceiver
{
    public Dictionary<string, InputEntry> Map = new Dictionary<string, InputEntry>();

    public void Copy(GamepadConfig config)
    {
        Map.Clear();

        foreach (var e in config.Map)
            Map.Add(e.Key, e.Value);
    }
    
    #region Unity Serialisation
    [HideInInspector] [SerializeField] private List<string> actions;
    [HideInInspector] [SerializeField] private List<InputEntry> entries;

    public void OnBeforeSerialize()
    {
        actions.Clear();
        entries.Clear();

        if (Map == null)
            return;

        foreach (var m in Map)
        { 
            actions.Add(m.Key);
            entries.Add(m.Value);
        }
    }

    public void OnAfterDeserialize()
    {
        Map = new Dictionary<string, InputEntry>();

        if (actions == null || actions.Count == 0)
            return;

        for (int i = 0; i < actions.Count; i++)
            Map.Add(actions[i], entries[i]);
    }
    #endregion
}
