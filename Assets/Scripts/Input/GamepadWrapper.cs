﻿using Rewired;
using System;
using UnityEngine;

public enum CharacterAction
{
    JUMP,
    DASH,
    ATTACK,
    GLORY,
}

[Serializable]
public class GamepadWrapper
{
    private static float AxisDeadZone = 0.3f;

    public int Id = 0;
    public bool IsConnected = false;
    public bool IsPlaying = false;

    public bool UseLeftStick = true;
    public bool UseRightStick = false;
    public bool UseDPad = false;

    private Vector2 leftStickAxis;
    private Vector2 rightStickAxis;    
    private Vector2 dpadAxis;
    
    public bool IsReady
    {
        get { return IsConnected && IsPlaying; }
    }

    public GamepadWrapper(int index, bool isConnected, bool isPlaying)
    {
        Id = index;
        IsConnected = isConnected;
        IsPlaying = isPlaying;

        rightStickAxis = leftStickAxis = dpadAxis = new Vector2();
    }

    #region Movement
    private Vector2 GetLeftStickAxis()
    {
        leftStickAxis.x = ReInput.players.GetPlayer(Id).GetAxis("MoveHorizontal");
        leftStickAxis.y = ReInput.players.GetPlayer(Id).GetAxis("MoveVertical");
        return leftStickAxis;
    }

    private Vector2 GetRightStickAxis()
    {
        rightStickAxis.x = ReInput.players.GetPlayer(Id).GetAxis("MoveHorizontal");
        rightStickAxis.y = ReInput.players.GetPlayer(Id).GetAxis("MoveVertical");
        return rightStickAxis;
    }

    private Vector2 GetDPadAxis()
    {
        dpadAxis.x = ReInput.players.GetPlayer(Id).GetAxis("MoveHorizontal");
        dpadAxis.y = ReInput.players.GetPlayer(Id).GetAxis("MoveVertical");
        return dpadAxis;
    }

    public Vector2 GetMovement()
    {
        Vector2 axis = 
            (UseLeftStick ? GetLeftStickAxis() : Vector2.zero) +
            (UseRightStick ? GetRightStickAxis() : Vector2.zero) +
            (UseDPad ? GetDPadAxis() : Vector2.zero);

        if (Mathf.Abs(axis.x) < AxisDeadZone) axis.x = 0f;
        if (Mathf.Abs(axis.y) < AxisDeadZone) axis.y = 0f;
        
        return axis;
    }
    #endregion

    #region Actions
    public bool GetMenu()
    {
        return ReInput.players.GetPlayer(Id).GetButtonDown("MENU");
    }

    public bool GetStartJump()
    {
        return ReInput.players.GetPlayer(Id).GetButtonDown("JUMP");
    }

    public bool GetStopJump()
    {
        return ReInput.players.GetPlayer(Id).GetButtonUp("JUMP");
    }

    public bool GetStartDash()
    {
        return ReInput.players.GetPlayer(Id).GetButtonDown("DASH");
    }

    public bool GetStopDash()
    {
        return ReInput.players.GetPlayer(Id).GetButtonUp("DASH");
    }

    public bool GetAttackUp()
    {
        return ReInput.players.GetPlayer(Id).GetButtonUp("ATTACK");
    }

    public bool GetAttackDown()
    {
        return ReInput.players.GetPlayer(Id).GetButtonDown("ATTACK");
    }

    public bool GetFallDown()
    {
        return ReInput.players.GetPlayer(Id).GetNegativeButtonDoublePressDown("MoveVertical");
    }
    
    public bool CheckJoinRequest()
    {
        return ReInput.players.GetPlayer(Id).GetButtonDown("SyncLeft") && ReInput.players.GetPlayer(Id).GetButtonDown("SyncRight");
    }
    #endregion
}