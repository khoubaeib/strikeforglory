﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelsManager : PersistentSingleton<LevelsManager>
{

    #region Inspector fields
    [SerializeField] [SceneName] private string preLoader;
    [SerializeField] [SceneName] private string splash;
    [SerializeField] [SceneName] private string pauseLevel;
    [SerializeField] [SceneName] private string mainMenuLevel;
    #endregion

    private string currentMatch = "";

    #region Preloader
    public void CheckPreloaderScene()
    {
        if (IsSceneLoaded(preLoader) && SceneManager.sceneCount > 1)
            return;

        SceneManager.LoadSceneAsync(splash, LoadSceneMode.Additive);
        //StartCoroutine(LoadMainMenu());
    }

    public static void LoadPreLoaderScene()
    {
        string preloader = "_preloader";
        Scene previous = SceneManager.GetActiveScene();

        SceneManager.LoadScene(preloader);
        SceneManager.LoadScene(previous.name, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(previous);
    }
    #endregion
    
    #region Main menu
    public bool IsMainMenu()
    {
        return IsSceneLoaded(mainMenuLevel);
    }
    
    public IEnumerator LoadMainMenu()
    {
        yield return Transition(mainMenuLevel, pauseLevel, splash, currentMatch);
        AudioManager.Instance.PlayMainMenuTheme();
        currentMatch = "";
    }
    #endregion

    #region Pause
    public IEnumerator LoadPause()
    {
        yield return SceneManager.LoadSceneAsync(pauseLevel, LoadSceneMode.Additive);
    }

    public IEnumerator UnloadPause()
    {
        if (!IsSceneLoaded(pauseLevel))
            yield break;

        yield return SceneManager.UnloadSceneAsync(pauseLevel);
    }
    #endregion
    
    #region Match
    public IEnumerator LoadMatch(string next)
    {
        yield return Instance.StartCoroutine(Instance.Transition(next, mainMenuLevel, pauseLevel, currentMatch));
        currentMatch = next;
    }
    #endregion

    #region Scene loading    
    private IEnumerator VSTransition(string next, params string[] previous)
    {
        yield return StartCoroutine(LoadingScreenManager.FadeVSScreenOut());
        yield return UnloadScenes(previous);
        yield return SceneManager.LoadSceneAsync(next, LoadSceneMode.Additive);
        SetActiveScene(next);
        yield return StartCoroutine(LoadingScreenManager.FadeVSScreenIn());
    }

    private IEnumerator Transition(string next, params string[] previous)
    {
        yield return StartCoroutine(LoadingScreenManager.FadeSceneOut());
        yield return UnloadScenes(previous);
        yield return SceneManager.LoadSceneAsync(next, LoadSceneMode.Additive);
        SetActiveScene(next);
        yield return StartCoroutine(LoadingScreenManager.FadeSceneIn());
    }

    public IEnumerator UnloadScenes(params string[] scenes)
    {
        if (scenes == null)
            yield break;

        foreach(var s in scenes)
        {
            if (s == null || s.Equals("") ||  !IsSceneLoaded(s))
                continue;
            
            yield return SceneManager.UnloadSceneAsync(s);
        }
    }

    public bool IsSceneLoaded(string scene)
    {
        int count = SceneManager.sceneCount;
        for (int i = 0; i < count; i++)
            if (SceneManager.GetSceneAt(i).name.Equals(scene))
                return true;

        return false;
    }

    private void SetActiveScene(string scene)
    {
        Scene s = SceneManager.GetSceneByName(scene);
        if (s.buildIndex == -1)
            return;

        SceneManager.SetActiveScene(s);
    }
    #endregion

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
