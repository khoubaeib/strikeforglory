﻿using UnityEngine;

public class EndOfMatchScreen : SlideSingleton<EndOfMatchScreen>
{

    public void RestartGame()
    {
        GameData.Instance.InitRoundsResult();
        SlideManager.Instance.SwitchLevel(GameData.Instance.GetSelectedLevel());
    }

    public void StageSelect()
    {
        SlideManager.Instance.SwitchMainMenu<StageSelectScreen>();
    }

    public void CharacterSelect()
    {
        SlideManager.Instance.SwitchMainMenu<CharacterSelectScreen>();
    }

    public void MainMenu()
    {
        SlideManager.Instance.SwitchMainMenu<MainMenuScreen>();
    }
}
