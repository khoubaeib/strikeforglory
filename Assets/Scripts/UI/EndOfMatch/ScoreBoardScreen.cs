﻿using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardScreen : SlideSingleton<ScoreBoardScreen>
{

    [SerializeField] private RoundUIEntry roundScore;
    [SerializeField] private RectTransform dataContainer;

    [SerializeField] private Button nextMatch;
    [SerializeField] private Button endOfMatch;

    private bool next = false;

    private void Start()
    {
        nextMatch.gameObject.SetActive(false);
        endOfMatch.gameObject.SetActive(false);
    }

    public void ResetEntries()
    {
        roundScore.ResetAnimation();
    }

    private void Update()
    {
        if (!next)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UISubmit"))
            {
                if (GameData.Instance.IsTheLastRound)
                    EndOfMatch();
                else
                    NextRound();
            }
        }
    }

    public IEnumerator ShowCurrentScore()
    {
        nextMatch.gameObject.SetActive(true);
        endOfMatch.gameObject.SetActive(false);

        yield return roundScore.Animate();
        roundScore.SetScore(GameData.Instance.GetPlayer1UIScore(), GameData.Instance.GetPlayer2UIScore());

        next = true;
    }

    public IEnumerator ShowFinalScore()
    {
        next = true;
        nextMatch.gameObject.SetActive(false);
        endOfMatch.gameObject.SetActive(true);
        
        yield return roundScore.Animate();
        roundScore.SetScore(GameData.Instance.GetPlayer1UIScore(), GameData.Instance.GetPlayer2UIScore(), true);
        int w = GameData.Instance.GameWinner;
        roundScore.SetWinner(w == 0, w == 1);
    }
    
    public void NextRound()
    {
        if (!next)
            return;
        
        next = false;
        nextMatch.gameObject.SetActive(false);
        endOfMatch.gameObject.SetActive(false);

        MatchManager.Instance.ContinueNextRound();
    }

    public void EndOfMatch()
    {
        if (!next)
            return;

        next = false;
        nextMatch.gameObject.SetActive(false);
        endOfMatch.gameObject.SetActive(false);

        SlideManager.Instance.Switch(EndOfMatchScreen.Slide);
    }
}
