﻿using Rewired;

public class StrikeScreen : SlideSingleton<StrikeScreen>
{
    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    public void GloryMode()
    {
        GameData.Instance.SetStrikeForGloryMode();
        SlideManager.Instance.Switch(CharacterSelectScreen.Slide);
    }

    public void FunMode()
    {
        GameData.Instance.SetStrikeForFunMode();
        SlideManager.Instance.Switch(CharacterSelectScreen.Slide);
    }

    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        if (!back)
            return;
        
        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
