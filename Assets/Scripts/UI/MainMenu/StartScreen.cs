﻿using Rewired;
using UnityEngine;

public class StartScreen : SlideSingleton<StartScreen>
{
    [SerializeField] private Animation pressAnyButton;

    private bool canGoToMain = false;
    

    private void Start()
    {
        Slide.OnAfterFadeOut += ActivateInput;
    }

    private void ActivateInput()
    {
        canGoToMain = true;
        pressAnyButton.Play();
    }
    
    private void Update()
    {
        if (!canGoToMain || !ReInput.controllers.GetAnyButton())
            return;

        SlideManager.Instance.Switch(MainMenuScreen.Slide);
        canGoToMain = false;
    }
}
