﻿using UnityEngine;

public class MainMenuScreen : SlideSingleton<MainMenuScreen>
{
    public void StrikeMode()
    {
        SlideManager.Instance.Switch(StrikeScreen.Slide);
    }

    public void RecRoom()
    {
        SlideManager.Instance.Switch(RecRoomScreen.Slide);
    }

    public void SurvivalMode()
    {
        GameData.Instance.Mode = MatchMode.Survival;
        SlideManager.Instance.Switch(SurvivalScreen.Slide);
    }

    public void HelpAndOptions()
    {
        SlideManager.Instance.Switch(HelpAndOptionsScreen.Slide);
    }

    public void ExtiGame()
    {
        LevelsManager.Instance.ExitGame();
    }
}
