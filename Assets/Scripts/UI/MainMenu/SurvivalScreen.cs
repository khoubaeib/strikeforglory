﻿using Rewired;
using UnityEngine;

public class SurvivalScreen : SlideSingleton<SurvivalScreen>
{
    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    public void EasyMode()
    {
        GameData.Instance.SetEasySurvivalMode();
        SlideManager.Instance.Switch(CharacterSelectScreen.Slide);
    }

    public void MediumMode()
    {
        GameData.Instance.SetMediumSurvivalMode();
        SlideManager.Instance.Switch(CharacterSelectScreen.Slide);
    }

    public void HardMode()
    {
        GameData.Instance.SetHardSurvivalMode();
        SlideManager.Instance.Switch(CharacterSelectScreen.Slide);
    }

    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        if (!back)
            return;
        
        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
