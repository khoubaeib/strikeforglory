﻿using Rewired;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StageSelectScreen : SlideSingleton<StageSelectScreen>
{
    [SerializeField] private Button firstHovered;
    [SerializeField] private Text stageName;
    
    public void HoverLevel(string level)
    {
        stageName.text = level;
    }

    public void SelectLevel(string level)
    {
        int i = GameData.Instance.GetLevelIndexByName(level) ;
        if (i == -1)
            return;

        GameData.Instance.SelectedLevel = i;
        LoadLevel();
    }

    private void LoadLevel()
    {
        GameData.Instance.InitRoundsResult();
        SlideManager.Instance.SwitchLevel(GameData.Instance.GetSelectedLevel());
    }

    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true );
        Slide.OnBeforeFadeIn += (() => back = false);

        firstHovered.GetComponent<EventTrigger>().OnSelect(null);
    }
    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }

    public void ReturnToPreviousScreen()
    {
        Slide last = SlideManager.Instance.PeekBeforeLast;
        if (last)
        {
            SlideManager.Instance.GoBack();
            return;
        }

        SlideManager.Instance.SetCustomHistroy(MainMenuScreen.Slide, StrikeScreen.Slide, CharacterSelectScreen.Slide, Slide);
        SlideManager.Instance.GoBack();
    }
}
