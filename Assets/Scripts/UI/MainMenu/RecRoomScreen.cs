﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Slide))]
public class RecRoomScreen : SlideSingleton<RecRoomScreen>
{   
    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }


    public void Library()
    {
        
    }

    public void Artwork()
    {
        
    }

    public void Videos()
    {
        
    }

    public void Credits()
    {
        SlideManager.Instance.Switch(CreditsScreen.Slide);
    }

    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }
    
    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
