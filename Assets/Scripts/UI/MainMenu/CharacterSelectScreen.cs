﻿using System;
using Rewired;
using Rewired.Integration.UnityUI;
using UnityEngine;
using UnityEngine.UI;


public class CharacterSelectScreen : SlideSingleton<CharacterSelectScreen>
{

    [SerializeField] private string horizontalAxis;
    [SerializeField] private string confirmButton;

    [SerializeField] private RectTransform[] charactersThumbnails;
    [SerializeField] private RectTransform[] playersSelector;
    [SerializeField] private Image[] playersSelection;
    [SerializeField] private Sprite[] NoSelections;
    [SerializeField] private Button confirmSelection;

    private int charactersCount;
    private int playersCount;

    private int[] hovers;
    private int[] selections;
    private bool back = false;

    private RewiredStandaloneInputModule inputHandler;
    
    public void Start()
    {
        inputHandler = FindObjectOfType<RewiredStandaloneInputModule>();
        
        Slide.OnAfterFadeOut += (() => { back = true;  inputHandler.UseAllRewiredGamePlayers = false; });
        Slide.OnBeforeFadeIn += (() => { back = false; inputHandler.UseAllRewiredGamePlayers = true; });

        RefreshPlayersSelection(-1);

        InputHandler.Instance.OnPlayerJoin += RefreshPlayersSelection;
        InputHandler.Instance.OnPlayerLeave += RefreshPlayersSelection;
    }

    private void RefreshPlayersSelection(int obj)
    {
        charactersCount = GameData.Instance.Characters.Length;
        playersCount = GameData.Instance.PlayersCount;

        selections = new int[playersCount];
        hovers = new int[playersCount];

        for (int i = 0; i < playersCount; i++)
        {
            if(GameData.Instance.SelectedCharacters[i] != -1) {
                SelectCharacter(i);
                HoverCharacter(i, GameData.Instance.SelectedCharacters[i]);
            } else {
                UnselectCharacter(i);
                HoverCharacter(i, 0); 
            }
        }

        confirmSelection.interactable = false;
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;

        InputHandler.Instance.OnPlayerJoin -= RefreshPlayersSelection;
        InputHandler.Instance.OnPlayerLeave -= RefreshPlayersSelection;
    }

    private void Update()
    {
        PlayersNavigation();
    }

    public void HoverCharacter(int player, int character)
    {
        if (selections[player] != -1)
            return;

        hovers[player] = Mathf.Clamp(character, 0, charactersCount - 1);
        playersSelector[player].SetParent(charactersThumbnails[hovers[player]], false);
    }
    
    public void SelectCharacter(int player)
    {
        selections[player] = hovers[player];
        GameData.Instance.SelectedCharacters[player] = selections[player];
        playersSelection[player].sprite = GameData.Instance.GetPlayerThumbnail(player);
        confirmSelection.interactable = AllPlayersConfirm();
    }

    public void UnselectCharacter(int player)
    {
        selections[player] = -1;
        playersSelection[player].sprite = NoSelections[player];
        GameData.Instance.SelectedCharacters[player] = -1;
        confirmSelection.interactable = AllPlayersConfirm();
    }

    private bool AllPlayersConfirm()
    {
        for (int i = 0; i < selections.Length; i++)
            if (selections[i] == -1)
                return false;

        return true;
    }
    
    public void ConfirmSelectedCharacters()
    {
        if (!AllPlayersConfirm())
            return;

        SlideManager.Instance.Switch(StageSelectScreen.Slide);
    }
    
    public void ReturnToPreviousScreen()
    {
        Slide last = SlideManager.Instance.PeekBeforeLast;
        if (last)
        {
            SlideManager.Instance.GoBack();
            return;
        }

        var mode = GameData.Instance.Mode;
        SlideManager.Instance.SetCustomHistroy(MainMenuScreen.Slide, mode == MatchMode.Survival ? SurvivalScreen.Slide : StrikeScreen.Slide, Slide);
        SlideManager.Instance.GoBack();
    }

    private void PlayersNavigation()
    {
        if (!back)
            return;

        for (int i = 0; i < ReInput.players.playerCount; i++)
        {
            Player p = ReInput.players.GetPlayer(i);
            if (p.GetButtonDown(horizontalAxis))
            {
                HoverCharacter(i, hovers[i] + 1);
                return;
            }

            if (p.GetNegativeButtonDown(horizontalAxis))
            {
                HoverCharacter(i, hovers[i] - 1);
                return;
            }
            
            if (p.GetButtonDown("UICancel"))
            {
                if (selections[i] == -1)
                    ReturnToPreviousScreen();
                else
                    UnselectCharacter(i);
                return;
            }

            if (p.GetButtonDown(confirmButton))
            {
                if (selections[i] == -1)
                    SelectCharacter(i);
                else
                    ConfirmSelectedCharacters();
                return;
            }
        }
    }
}
