﻿using System.Collections;

public class MainMenuManager : Singleton<MainMenuManager>
{
    private IEnumerator Start()
    {
        yield return StartCoroutine(SlideManager.Instance.SwitchRoutine(StartScreen.Slide));
    }
}
