﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Slide))]
public class CreditsScreen : SlideSingleton<CreditsScreen>
{
    [SerializeField] private Animation creditsScroll;
    
    private bool back = false;

    private void Start()
    {
        Slide.OnBeforeFadeOut += (() => { creditsScroll.GetComponent<RectTransform>().anchoredPosition = Vector2.up * -650 ; creditsScroll.Stop(); });
        Slide.OnAfterFadeOut += (() => { back = true; creditsScroll.Play(); });
        Slide.OnBeforeFadeIn += (() => back = false);
        Slide.OnAfterFadeIn += (() => creditsScroll.Stop());
    }

    private void OnDestroy()
    {
        Slide.OnBeforeFadeIn = null;
        Slide.OnBeforeFadeOut = null;
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
        Slide.OnAfterFadeIn = null;
    }


    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }
    
    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
