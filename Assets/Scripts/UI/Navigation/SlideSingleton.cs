﻿using UnityEngine;

[RequireComponent(typeof(Slide))]
public class SlideSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance = null;
    private static Slide slide = null;

    public static Slide Slide
    {
        get { return slide ? slide : slide = instance.gameObject.GetComponent<Slide>(); }
    }

    public static T Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        CheckInstance();
    }

    protected bool CheckInstance()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return false;
        }

        instance = FindObjectOfType<T>();
        return true;
    }
}