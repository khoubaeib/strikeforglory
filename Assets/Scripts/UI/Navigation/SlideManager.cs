﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideManager : PersistentSingleton<SlideManager>
{

    private Stack<Slide> navigation = new Stack<Slide>();
    private bool isSwitching = false;

    #region Slide navigation
    public Slide PeekLast
    {
        get{ return navigation.Count > 0 ? navigation.Peek() : null; }
    }

    public Slide PeekBeforeLast
    {
        get { return navigation.Count > 1 ? navigation.ToArray()[navigation.Count - 1] : null; }
    }
    
    public Slide PopLast
    {
        get { return navigation.Count > 0 ? navigation.Pop() : null; }
    }

    public void ClearHistory()
    {
        navigation.Clear();
    }

    public void SetCustomHistroy(params Slide[] history)
    {
        navigation.Clear();
        foreach (var h in history)
            navigation.Push(h);
    }

    public void GoBack()
    {
        if (navigation.Count == 0)
        {
            StartCoroutine(_SwitchBackward(MainMenuScreen.Slide));
            return;
        }

        StartCoroutine(_SwitchBackward(null));
    }
    #endregion

    #region Slides in same scene
    public void Switch(Slide to) 
    {
        if (!to)
            return;

        StartCoroutine(SwitchRoutine(to));
    }

    public IEnumerator SwitchRoutine(Slide to)
    {
        if (!to)
            yield break;
        
        yield return _SwitchForward(to);
    }

    private IEnumerator _SwitchForward(Slide to)
    {
        if (isSwitching)
            yield break;

        Slide from = PeekLast;
        if (from && from == to)
            yield break;

        isSwitching = true;

        if (from)
            StartCoroutine(from.FadeIn());
        yield return StartCoroutine(to.FadeOut());

        navigation.Push(to);
        isSwitching = false;
    }

    private IEnumerator _SwitchBackward(Slide defaultSlide)
    {
        Slide from = PopLast;
        Slide to = PeekLast;

        if (isSwitching)
            yield break;

        isSwitching = true;

        if (from) StartCoroutine(from.FadeIn());
        to = to ? to : defaultSlide;
        yield return StartCoroutine(to.FadeOut());

        isSwitching = false;
    }
    #endregion

    #region Pause => MainMenu 
    public void SwitchMainMenu<T>() where T: SlideSingleton<T>
    {
        StartCoroutine(_SwitchMainMenu<T>());
    }

    public IEnumerator _SwitchMainMenu<T>() where T : SlideSingleton<T>
    {
        if (isSwitching)
            yield break;

        isSwitching = true;

        Slide from = PopLast;
        if (from) StartCoroutine(from.FadeIn());
        yield return LevelsManager.Instance.LoadMainMenu();
        Slide slideTo = SlideSingleton<T>.Slide;
        yield return StartCoroutine(slideTo.FadeOut());
        navigation.Clear();
        navigation.Push(slideTo);

        isSwitching = false;
    }
    #endregion

    #region Main menu => Level
    public void SwitchLevel(string levelName)
    {
        StartCoroutine(_SwitchLevel(levelName));
    }

    public IEnumerator _SwitchLevel(string levelName)
    {
        if (isSwitching)
            yield break;

        isSwitching = true;
        
        Slide from = PopLast;
        if (from) yield return StartCoroutine(from.FadeIn());
        if (LevelsManager.Instance.IsSceneLoaded(levelName))
        {
            yield return StartCoroutine(LevelsManager.Instance.UnloadScenes(levelName));
        }

        yield return LevelsManager.Instance.LoadMatch(levelName);
        navigation.Clear();
        
        isSwitching = false;
    }
    #endregion

    #region Level <=> Pause
    public IEnumerator SwitchPauseMenu()
    {
        yield return StartCoroutine(_SwitchPauseMenu());
    }

    public IEnumerator ExitPauseMenu()
    {
        yield return StartCoroutine(_ExitPauseMenu());
    }

    public IEnumerator _SwitchPauseMenu()
    {
        if (isSwitching)
            yield break;

        isSwitching = true;

        navigation.Clear();
        yield return LevelsManager.Instance.LoadPause();
        Slide to = PauseMenuScreen.Slide;
        yield return StartCoroutine(to.FadeOut());
        navigation.Push(to);

        isSwitching = false;
    }

    public IEnumerator _ExitPauseMenu()
    {
        if (isSwitching)
            yield break;

        isSwitching = true;

        navigation.Clear();
        yield return StartCoroutine(PauseMenuScreen.Slide.FadeIn());
        yield return LevelsManager.Instance.UnloadPause();
        
        isSwitching = false;
    }
    #endregion

}
