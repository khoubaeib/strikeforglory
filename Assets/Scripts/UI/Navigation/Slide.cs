﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class Slide : MonoBehaviour
{
    #region Events
    public Action OnBeforeFadeIn;
    public Action OnAfterFadeIn;
    public Action OnBeforeFadeOut;
    public Action OnAfterFadeOut;
    #endregion

    [SerializeField] private bool setInactiveOnStart = true;
    [SerializeField] private Selectable[] selectables;

    #region
    private float fadeOutDuration = .3f;
    private float fadeInDuration = .3f;
    private float swipSpeed = 10f;
    private float epsilon = 5f;
    private float fadeOutPosition = 650f;
    private float fadeInPosition = -650f;
    private float startPosition;
    #endregion

    #region Components
    private CanvasGroup group;
    private CanvasGroup GetGroup()
    {
        return group ? group : group = GetComponent<CanvasGroup>();
    }
    
    private RectTransform rectangle;
    private RectTransform GetRectangle()
    {
        return rectangle ? rectangle : rectangle = GetComponent<RectTransform>();
    }
    #endregion

    private void Start()
    {
        if(setInactiveOnStart)
            SetSelectables(false);
    }


    public IEnumerator Fade(float startAlpha, float finalAlpha, float startPosition, float finalPosition, float duration)
    {
        GetGroup().blocksRaycasts = false;

        SetPositionX(startPosition);
        GetGroup().alpha = startAlpha;

        float fadeSpeed = Mathf.Abs(GetGroup().alpha - finalAlpha) / duration;

        bool alphaBlended = false, positionBlended = false;
        while (!alphaBlended || !positionBlended)
        {
            alphaBlended = Mathf.Approximately(GetGroup().alpha, finalAlpha);
            if (!alphaBlended) GetGroup().alpha = Mathf.MoveTowards(GetGroup().alpha, finalAlpha, fadeSpeed * Time.deltaTime);

            positionBlended = Mathf.Abs(GetRectangle().anchoredPosition.x - finalPosition) < epsilon;
            if (!positionBlended) SetPositionX(Mathf.Lerp(GetRectangle().anchoredPosition.x, finalPosition, swipSpeed * Time.deltaTime));
            
            yield return null;
        }
        
        SetPositionX(finalPosition);
        GetGroup().alpha = finalAlpha;
        
    }
    
    //EXIT
    public IEnumerator FadeIn()
    {
        if (OnBeforeFadeIn != null)
            OnBeforeFadeIn();

        GetGroup().blocksRaycasts = true;
        yield return StartCoroutine(Fade(1f, 0f, 0f, fadeInPosition, fadeInDuration));
        GetGroup().interactable = false;

        SetSelectables(false);

        if (OnAfterFadeIn != null)
            OnAfterFadeIn();
    }

    //ENTRY
    public IEnumerator FadeOut()
    {
        if (OnBeforeFadeOut != null)
            OnBeforeFadeOut();

        SetSelectables(true);

        SelectFirstSelectable();

        GetGroup().interactable = true;
        yield return StartCoroutine(Fade(0f, 1f, fadeOutPosition, 0f, fadeOutDuration));
        GetGroup().blocksRaycasts = true;

        if (OnAfterFadeOut != null)
            OnAfterFadeOut();
    }

    private void SetPositionX(float x)
    {
        GetRectangle().anchoredPosition = new Vector3(x, GetRectangle().anchoredPosition.y);
        Canvas.ForceUpdateCanvases();
    }

    private void SetSelectables(bool state)
    {
        foreach (var s in selectables)
            if (s)
                s.gameObject.SetActive(state);
    }

    public void SelectFirstSelectable()
    {
        if (selectables.Length == 0)
            return;
        
        Selectable s = null;
        foreach (var se in selectables)
        {
            if (se && se.interactable)
            { 
                s = se;
                break;
            }
        }

        if (!s)
            return;

        s.gameObject.SetActive(false);
        s.Select();
        s.OnSelect(null);
        s.gameObject.SetActive(true);
    }

 }
