﻿using System.Collections;
using UnityEngine;

public class LoadingScreenManager : PersistentSingleton<LoadingScreenManager>
{
    [SerializeField] private Animation loadingAnimation;
    [SerializeField] private CanvasGroup vsScreen;
    [SerializeField] private CanvasGroup loadingScreen;

    [SerializeField] private float fadeInDuration = 2f;
    [SerializeField] private float fadeOutDuration = 2f;
    
    private bool isFading = false;
    
    protected IEnumerator Fade(CanvasGroup group, float finalAlpha, float duration)
    {
        isFading = true;
        group.blocksRaycasts = true;
        float fadeSpeed = Mathf.Abs(group.alpha - finalAlpha) / duration;
        while (!Mathf.Approximately(group.alpha, finalAlpha))
        {
            group.alpha = Mathf.MoveTowards(group.alpha, finalAlpha, fadeSpeed * Time.deltaTime);
            yield return null;
        }
        group.alpha = finalAlpha;
        isFading = false;
        group.blocksRaycasts = false;
    }

    
    public static IEnumerator FadeSceneIn()
    {
        if (Instance.isFading)
            yield break;

        yield return Instance.StartCoroutine(Instance.Fade(Instance.loadingScreen, 0f, Instance.fadeInDuration));
        Instance.loadingScreen.gameObject.SetActive(false);
    }

    public static IEnumerator FadeSceneOut()
    {
        if (Instance.isFading)
            yield break;

        Instance.loadingScreen.gameObject.SetActive(true);
        Instance.loadingAnimation.Play();
        yield return Instance.StartCoroutine(Instance.Fade(Instance.loadingScreen, 1f, Instance.fadeOutDuration));
    }

    public static IEnumerator FadeVSScreenIn()
    {
        if (Instance.isFading)
            yield break;

        yield return Instance.StartCoroutine(Instance.Fade(Instance.vsScreen, 0f, Instance.fadeInDuration));
        Instance.vsScreen.gameObject.SetActive(false);
    }

    public static IEnumerator FadeVSScreenOut()
    {
        if (Instance.isFading)
            yield break;

        Instance.vsScreen.gameObject.SetActive(true);
        Instance.loadingAnimation.Play();
        yield return Instance.StartCoroutine(Instance.Fade(Instance.vsScreen, 1f, Instance.fadeOutDuration));
    }
}
