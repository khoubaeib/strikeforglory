﻿using UnityEngine;
using UnityEngine.UI;

public class ColorShifter : MonoBehaviour {

    public float Speed = 1;

    private Image image;
    private Color c;

    void Start()
    {
        image = GetComponent<Image>();
    }

    void Update()
    {
        c = HSBColor.ToColor(new HSBColor(Mathf.PingPong(Time.time * Speed, 1), 1, 1));
        c.a = image.color.a;
        image.color = c;
    }

}
