﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MatchUIManager : Singleton<MatchUIManager>
{

    [SerializeField] private Animation UIMaskAnimation;
    [SerializeField] private GameObject roundCount;
    [SerializeField] private Text roundCountText;
    [SerializeField] private GameObject fightAnnoucement;
    [SerializeField] private GameObject playerWinAnnouncement;
    [SerializeField] private Text playerWinAnnouncementText;

    [SerializeField] private float startDelay;
    [SerializeField] private float waitToFightDelay;
    [SerializeField] private float waitToRestartDelay;


    public void SetRoundCount(int i)
    {
        roundCountText.text = "ROUND " + (i + 1);
    }

    public void SetPlayerWin(int i)
    {
        playerWinAnnouncementText.text = "PLAYER " + (i + 1) + " WINS !";
    }


    public IEnumerator MatchBegin()
    {
        yield return new WaitForSeconds(startDelay);
        yield return ShowRoundCount();
        yield return new WaitForSeconds(waitToFightDelay);
        yield return ShowFightAnnouncement();
    }

    public IEnumerator MatchEnd()
    {
        yield return ShowPlayerWin();
        yield return new WaitForSeconds(waitToRestartDelay);
    }


    private IEnumerator ShowRoundCount()
    {
        roundCount.SetActive(true);
        fightAnnoucement.SetActive(false);
        playerWinAnnouncement.SetActive(false);

        yield return StartCoroutine(Animate());
    }

    private IEnumerator ShowFightAnnouncement()
    {
        roundCount.SetActive(false);
        fightAnnoucement.SetActive(true);
        playerWinAnnouncement.SetActive(false);

        yield return StartCoroutine(Animate());
    }

    private IEnumerator ShowPlayerWin()
    {
        roundCount.SetActive(false);
        fightAnnoucement.SetActive(false);
        playerWinAnnouncement.SetActive(true);

        yield return StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        UIMaskAnimation.Play("Opening");
        yield return new WaitForSeconds(UIMaskAnimation.clip.averageDuration);
    }
}
