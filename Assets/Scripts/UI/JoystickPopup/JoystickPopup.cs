﻿using System.Collections;
using UnityEngine;

public class JoystickPopup : PersistentSingleton<JoystickPopup>, IRequireSetup
{
    [SerializeField] private JoystickMessage message;
    [SerializeField] private float showAlertTime = 5f;

    private Coroutine alertRoutine = null;

    public void Setup ()
    {
        InputHandler.Instance.OnConnectGamepad += OnConnectGamepad;
        InputHandler.Instance.OnDisconnectGamepad += OnDisconnectGamepad;
    }

    private void OnConnectGamepad(int index)
    {
        StopAlertRoutine();
        alertRoutine = StartCoroutine(AlertRoutine(true, LevelsManager.Instance.IsMainMenu() && SlideManager.Instance.PeekLast != InputOptionsScreen.Slide, index));
    }

    private void OnDisconnectGamepad(int index)
    {
        StopAlertRoutine();
        alertRoutine = StartCoroutine(AlertRoutine(false));

        if (LevelsManager.Instance.IsMainMenu())
            return;

        MatchManager.Instance.PauseGame();
    }

    private void ConfigureController(int index = -1)
    {
        if (SlideManager.Instance.PeekLast == InputOptionsScreen.Slide)
            return;

        InputOptionsScreen.Instance.SetControllerConfiguration(index);
        SlideManager.Instance.Switch(InputOptionsScreen.Slide);
        SlideManager.Instance.SetCustomHistroy(MainMenuScreen.Slide, HelpAndOptionsScreen.Slide, GameOptionsScreen.Slide, InputOptionsScreen.Slide);
    }

    private void StopAlertRoutine()
    {
        if (alertRoutine == null)
            return;

        StopCoroutine(alertRoutine);
        alertRoutine = null;
    }

    private IEnumerator AlertRoutine(bool connected, bool configureGamepad = false, int gamepadIndex = -1)
    {
        yield return message.Show(connected, configureGamepad);
        yield return new WaitForSeconds(showAlertTime);
        /*else
        {
            float time = showAlertTime;
            while (time > 0)
            {
                if (InputHandler.Instance.Gamepads[gamepadIndex].CheckJoinRequest())
                {
                    time = 0;
                    ConfigureController(gamepadIndex);
                }

                time -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }*/

        yield return message.Hide();
    }
}
