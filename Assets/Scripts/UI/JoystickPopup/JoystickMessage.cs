﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class JoystickMessage : MonoBehaviour
{

    #region Inspector
    [SerializeField] private GameObject connectedMessage;
    [SerializeField] private GameObject disconnectedMessage;
    [SerializeField] private GameObject holdToEnterGame;
    #endregion

    #region Component
    private CanvasGroup group;
    private CanvasGroup GetGroup()
    {
        return group ? group : group = GetComponent<CanvasGroup>();
    }

    private RectTransform rectangle;
    private RectTransform GetRectangle()
    {
        return rectangle ? rectangle : rectangle = GetComponent<RectTransform>();
    }
    #endregion

    #region 
    private float fadeOutDuration = .3f;
    private float fadeInDuration = .3f;
    private float swipSpeed = 10f;
    private float epsilon = 5f;
    private float fadeOutPosition = 200f;
    private float fadeInPosition = 200f;
    #endregion

    public IEnumerator Show(bool connected, bool configureGamepad)
    {
        connectedMessage.SetActive(connected);
        disconnectedMessage.SetActive(!connected);
        holdToEnterGame.SetActive(connected && !configureGamepad);

        GetGroup().interactable = true;
        yield return StartCoroutine(Fade(0f, 1f, fadeOutPosition, 0f, fadeOutDuration));
        GetGroup().blocksRaycasts = true;
    }

    public IEnumerator Hide()
    {
        GetGroup().blocksRaycasts = true;
        yield return StartCoroutine(Fade(1f, 0f, 0f, fadeInPosition, fadeInDuration));
        GetGroup().interactable = false;

        connectedMessage.SetActive(false);
        disconnectedMessage.SetActive(false);
        holdToEnterGame.SetActive(false);
    }

    public IEnumerator Fade(float startAlpha, float finalAlpha, float startPosition, float finalPosition, float duration)
    {
        GetGroup().blocksRaycasts = false;

        SetPositionX(startPosition);
        GetGroup().alpha = startAlpha;

        float fadeSpeed = Mathf.Abs(GetGroup().alpha - finalAlpha) / duration;

        bool alphaBlended = false, positionBlended = false;
        while (!alphaBlended || !positionBlended)
        {
            alphaBlended = Mathf.Approximately(GetGroup().alpha, finalAlpha);
            if (!alphaBlended) GetGroup().alpha = Mathf.MoveTowards(GetGroup().alpha, finalAlpha, fadeSpeed * Time.deltaTime);

            positionBlended = Mathf.Abs(GetRectangle().anchoredPosition.x - finalPosition) < epsilon;
            if (!positionBlended) SetPositionX(Mathf.Lerp(GetRectangle().anchoredPosition.x, finalPosition, swipSpeed * Time.deltaTime));

            yield return null;
        }

        SetPositionX(finalPosition);
        GetGroup().alpha = finalAlpha;

    }

    private void SetPositionX(float x)
    {
        GetRectangle().anchoredPosition = new Vector3(x, GetRectangle().anchoredPosition.y);
        Canvas.ForceUpdateCanvases();
    }

}
