﻿using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
 
 public class ScrollRectAutoScroll : MonoBehaviour
{

    List<Selectable> selectables = new List<Selectable>();

    int elementCount;

    #region Properties
    ScrollRect ScrollRectComponent { get; set; }
    #endregion

    void OnEnable()
    {
        if (ScrollRectComponent)
        {
            ScrollRectComponent.content.GetComponentsInChildren(selectables);
            elementCount = selectables.Count;
        }
    }

    void Awake()
    {
        ScrollRectComponent = GetComponent<ScrollRect>();
    }

    void Start()
    {
        ScrollRectComponent.content.GetComponentsInChildren(selectables);
        elementCount = selectables.Count;
    }

    void Update()
    {
        if (elementCount == 0)
            return;


        foreach (var p in ReInput.players.Players)
        {
            bool input = p.GetButtonDown("UIHorizontal") || p.GetNegativeButtonDown("UIHorizontal") || p.GetButtonDown("UIVertical") || p.GetNegativeButtonDown("UIVertical");
            if (!input)
                continue;

            FocusSelectedEntry();
        }
    }


    void FocusSelectedEntry()
    {
        int selectedIndex = -1;
        Selectable selectedElement = EventSystem.current.currentSelectedGameObject ? EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>() : null;

        if (selectedElement)
            selectedIndex = selectables.IndexOf(selectedElement);

        if (selectedIndex > -1)
            ScrollRectComponent.verticalNormalizedPosition = 1 - (selectedIndex / ((float)elementCount - 1));
    }

}