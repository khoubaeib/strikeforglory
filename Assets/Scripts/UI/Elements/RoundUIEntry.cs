﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RoundUIEntry : MonoBehaviour {

    [SerializeField] private float swipSpeed = 10f;
    [SerializeField] private float animationOffset = 400;

    [SerializeField] private RectTransform Player1Container;
    [SerializeField] private RectTransform Player2Container;

    [SerializeField] private Text Player1Text;
    [SerializeField] private Text Player2Text;

    private float finalPosition1;
    private float finalPosition2;

    private void Start()
    {
        finalPosition1 = Player1Container.anchoredPosition.x;
        finalPosition2 = Player2Container.anchoredPosition.x;

        ResetAnimation();
    }

    public void SetScore(string player1, string player2, bool finalScore = false)
    {
        Player1Text.text = player1;
        Player2Text.text = player2;
    }

    public void SetWinner(bool player1, bool player2)
    {
        Player1Container.GetComponent<ColorShifter>().enabled = player1;
        Player2Container.GetComponent<ColorShifter>().enabled = player2;
    }

    public IEnumerator Animate()
    {
        SetPositionX(Player1Container, finalPosition1 - animationOffset);
        SetPositionX(Player2Container, finalPosition2 + animationOffset);

        while (Mathf.Abs(Player1Container.anchoredPosition.x - finalPosition1) > 1f)
        {
            SetPositionX(Player1Container, Mathf.Lerp(Player1Container.anchoredPosition.x, finalPosition1, swipSpeed * Time.deltaTime));
            SetPositionX(Player2Container, Mathf.Lerp(Player2Container.anchoredPosition.x, finalPosition2, swipSpeed * Time.deltaTime));
            yield return null;
        }

        SetPositionX(Player1Container, finalPosition1);
        SetPositionX(Player2Container, finalPosition2);
    }

    public void ResetAnimation()
    {
        Player1Container.GetComponent<ColorShifter>().enabled = false;
        Player2Container.GetComponent<ColorShifter>().enabled = false;
        Player1Text.color = Color.black;
        Player2Text.color = Color.black;

        SetPositionX(Player1Container, finalPosition1 - animationOffset);
        SetPositionX(Player2Container, finalPosition2 + animationOffset);
    }

    private void SetPositionX(RectTransform sprite, float x)
    {
        sprite.anchoredPosition = new Vector3(x, sprite.anchoredPosition.y);
        Canvas.ForceUpdateCanvases();
    }
}
