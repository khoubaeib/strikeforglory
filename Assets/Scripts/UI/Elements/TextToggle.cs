﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TextToggle : MonoBehaviour
{
    [SerializeField] private bool isOn;
    [SerializeField] Text targetLabel;
    
    private UnityAction<bool> onValueChanged;
    private Color defaultColor;
    public bool IsOn
    {
        get { return isOn; }
        set
        {
            isOn = value;
            UpdateTargetLabel();
            if (onValueChanged != null)
                onValueChanged(isOn);
        }
    }

    public void AddListener(UnityAction<bool> listener)
    {
        onValueChanged += listener;
    }

    public void RemoveListener(UnityAction<bool> listener)
    {
        onValueChanged -= listener;
    }

    private void UpdateTargetLabel()
    {
        targetLabel.text = isOn ? "ON" : "OFF";
    }

    public void OnClickToChange()
    {
        IsOn = !isOn;

    }

    void Start ()
    {
        defaultColor = targetLabel.color;
        UpdateTargetLabel();
    }

    public void OnSelect()
    {
        targetLabel.color = GetComponent<Button>().colors.highlightedColor;
    }

    public void OnDeselect()
    {
        targetLabel.color = defaultColor;
    }
}
