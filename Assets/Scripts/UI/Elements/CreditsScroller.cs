﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScroller : MonoBehaviour
{
    public void GoBack()
    {
        CreditsScreen.Instance.ReturnToPreviousScreen();
    }
}
