﻿using Rewired;

public class PauseMenuScreen : SlideSingleton<PauseMenuScreen>
{

    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);
    }

    public void ResumeGame()
    {
        MatchManager.Instance.ResumeGame();
    }

    public void RestartGame()
    {
        MatchManager.Instance.DestroyAllPlayers();
        GameData.Instance.InitRoundsResult();
        SlideManager.Instance.SwitchLevel(GameData.Instance.GetSelectedLevel());
    }

    public void StageSelect()
    {
        SlideManager.Instance.SwitchMainMenu<StageSelectScreen>();
    }

    public void CharacterSelect()
    {
        SlideManager.Instance.SwitchMainMenu<CharacterSelectScreen>();
    }

    public void HelpAndOptions()
    {
        SlideManager.Instance.Switch(HelpAndOptionsScreen.Slide);
    }

    public void MainMenu()
    {
        SlideManager.Instance.SwitchMainMenu<MainMenuScreen>();
    }

    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                MatchManager.Instance.ResumeGame();
        }
    }
}
