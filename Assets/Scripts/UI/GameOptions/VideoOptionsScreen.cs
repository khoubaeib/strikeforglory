﻿using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class VideoOptionsScreen : SlideSingleton<VideoOptionsScreen>
{
    [SerializeField] private Slider BrightnessSlider;

    [SerializeField] private Dropdown ResolutionDropBox;
    [SerializeField] private Button confirmResolution;
    [SerializeField] private Button cancelResolution;

    [SerializeField] private TextToggle FullScreenToggle;
    [SerializeField] private TextToggle VSyncToggle;

    private List<Resolution> resolutions;

    private VideoSettingsData videoData;

    private bool back = false;
    
    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);

        LoadPersistantData();
        SubscribeToValuesChanges();
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    private void LoadPersistantData()
    {
        videoData = new VideoSettingsData(true);

        GenerateResolutions();
        GetVideoOptionsValues();
    }

    private int GetCurrentResolutionIndex()
    {
        for (int i = 0; i < resolutions.Count; i++)
            if (resolutions[i].Equals(videoData.ScreenResolution))
                return i;

        return -1;
    }

    private void GenerateResolutions()
    {
        resolutions = new List<Resolution>();
        foreach (var r in Screen.resolutions)
            if(r.refreshRate >= 59)
                resolutions.Add(r);

        ResolutionDropBox.ClearOptions();
        ResolutionDropBox.AddOptions(resolutions.Select(r => r.width + " X " + r.height + " X " + r.refreshRate).ToList());
    }

    private void GetVideoOptionsValues()
    {
        GetBrightness();
        GetResolutions();
        GetFullScreen();
        GetVSync();
    }
    
    private void GetBrightness()
    {
        CameraEffects.Brightness = videoData.Brightness;
        BrightnessSlider.value = videoData.Brightness;
    }

    private void GetResolutions()
    {
        Screen.SetResolution(videoData.ScreenResolution.width, videoData.ScreenResolution.height, videoData.FullScreen, videoData.ScreenResolution.refreshRate);
        int ri = GetCurrentResolutionIndex();
        if (ri == -1)
            return;

        ResolutionDropBox.value = ri;
        ResolutionDropBox.RefreshShownValue();
    }

    private void GetFullScreen()
    {
        Screen.fullScreen = videoData.FullScreen;
        FullScreenToggle.IsOn = videoData.FullScreen;
    }

    private void GetVSync()
    {
        QualitySettings.vSyncCount = videoData.VSync ? 1 : 0;
        VSyncToggle.IsOn = videoData.VSync;
    }

    private void SubscribeToValuesChanges()
    {
        BrightnessSlider.onValueChanged.AddListener(OnBrightnessValueChanged);
        ResolutionDropBox.onValueChanged.AddListener(OnResolutionsValueChanged);
        FullScreenToggle.AddListener(OnFullScreenValueChanged);
        VSyncToggle.AddListener(OnVSyncValueChanged);
        confirmResolution.onClick.AddListener(OnConfirmResolution);
        cancelResolution.onClick.AddListener(OnCancelResolution);
    }

    private void OnBrightnessValueChanged(float value)
    {
        CameraEffects.Brightness = videoData.Brightness;
        videoData.SaveData();
    }

    private void OnResolutionsValueChanged(int value)
    {
        if (resolutions[value].Equals(videoData.ScreenResolution))
            return;

        confirmResolution.gameObject.SetActive(true);
        cancelResolution.gameObject.SetActive(true);
    }

    private void OnConfirmResolution()
    {
        Resolution r = resolutions[ResolutionDropBox.value];
        Screen.SetResolution(r.width, r.height, videoData.FullScreen, r.refreshRate);
        videoData.ScreenResolution = r;
        videoData.SaveData();

        confirmResolution.gameObject.SetActive(false);
        cancelResolution.gameObject.SetActive(false);

        ResolutionDropBox.gameObject.SetActive(false);
        ResolutionDropBox.Select();
        ResolutionDropBox.OnSelect(null);
        ResolutionDropBox.gameObject.SetActive(true);
    }

    private void OnCancelResolution()
    {
        LoadPersistantData();
        confirmResolution.gameObject.SetActive(false);
        cancelResolution.gameObject.SetActive(false);

        ResolutionDropBox.gameObject.SetActive(false);
        ResolutionDropBox.Select();
        ResolutionDropBox.OnSelect(null);
        ResolutionDropBox.gameObject.SetActive(true);
    }

    private void OnFullScreenValueChanged(bool value)
    {
        Screen.fullScreen = value;
        videoData.SaveData();
    }

    private void OnVSyncValueChanged(bool value)
    {
        QualitySettings.vSyncCount = value ? 1 : 0;
        videoData.SaveData();
    }

    public void ReturnToPreviousScreen()
    {
        LoadPersistantData();
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
