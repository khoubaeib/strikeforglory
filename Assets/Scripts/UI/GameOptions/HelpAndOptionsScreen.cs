﻿using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpAndOptionsScreen : SlideSingleton<HelpAndOptionsScreen>
{

    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    public void Help()
    {
    }

    public void Options()
    {
        SlideManager.Instance.Switch(GameOptionsScreen.Slide);
    }

    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        if (!back)
            return;
        
        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
