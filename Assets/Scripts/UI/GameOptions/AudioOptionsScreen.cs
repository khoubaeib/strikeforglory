﻿using Rewired;
using UnityEngine;
using UnityEngine.UI;

public class AudioOptionsScreen : SlideSingleton<AudioOptionsScreen>
{
    [SerializeField] private Slider Master;
    [SerializeField] private Slider Effects;
    [SerializeField] private Slider Music;

    private AudioSettingsData audioData;

    private bool back = false;
    
    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);

        GetAudioSlidersValues();
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    private void GetAudioSlidersValues()
    {
        audioData = new AudioSettingsData(true);
        AudioManager.Instance.ApplySettings(audioData);

        Master.value = audioData.Master;
        Effects.value = audioData.Effects;
        Music.value = audioData.Music;
    }

    public void OnMasterAudioValueChange(float value)
    {
        AudioManager.Instance.SetMasterVolume(value);
    }

    public void OnEffectsAudioValueChange(float value)
    {
        AudioManager.Instance.SetEffectsVolume(value);
    }

    public void OnMusicAudioValueChange(float value)
    {
        AudioManager.Instance.SetMusicAudio(value);
    }
    
    public void ReturnToPreviousScreen()
    {
        audioData.SaveData();
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
