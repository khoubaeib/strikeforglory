﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralOptionsScreen : SlideSingleton<GeneralOptionsScreen>
{
    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
