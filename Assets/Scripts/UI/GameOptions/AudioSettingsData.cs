﻿public class AudioSettingsData : Data<float, float, float>, IPersistantSettings 
{
    public float Master
    {
        get { return value0; }
        set { value0 = value; }
    }

    public float Music
    {
        get { return value1; }
        set { value1 = value; }
    }

    public float Effects
    {
        get { return value2; }
        set { value2 = value; }
    }

    
    public AudioSettingsData(bool readPersistantData = true) :
        base("AUDIO_DATA", 1f, 1f, 01f)
    {
        if (readPersistantData)
            ReadData();
    }

    public void ReadData()
    {
        AudioSettingsData data = PersistenceManager.GetData<AudioSettingsData>(id);
        if (data == null)
            return;

        Master = data.Master;
        Music = data.Music;
        Effects = data.Effects;
    }

    public void SaveData()
    {
        PersistenceManager.UpdateData(this);
    }
    

}
