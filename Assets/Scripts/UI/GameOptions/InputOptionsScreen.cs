﻿using Rewired;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public enum GamepadButton
{
    Left, Right,
    A, B, X, Y,
}

public class InputOptionsScreen : SlideSingleton<InputOptionsScreen>
{
    [SerializeField] private Dropdown leftButton;
    [SerializeField] private Dropdown rightButton;
    [SerializeField] private Dropdown AButton;
    [SerializeField] private Dropdown BButton;
    [SerializeField] private Dropdown XButton;
    [SerializeField] private Dropdown YButton;

    [SerializeField] private Toggle useLeftStick;
    [SerializeField] private Toggle useRightStick;
    [SerializeField] private Toggle useDPad;

    [SerializeField] private Text playerNumber;
    [SerializeField] private Button playerNext;
    [SerializeField] private Button playerPrevious;

    private int currentPlayer = 0;

    private bool back = false;
    
    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);

        GeneratActionsList();

        leftButton.onValueChanged.AddListener(OnLeftButtonValueChanged);
        rightButton.onValueChanged.AddListener(OnLeftButtonValueChanged);
        AButton.onValueChanged.AddListener(OnLeftButtonValueChanged);
        BButton.onValueChanged.AddListener(OnLeftButtonValueChanged);
        XButton.onValueChanged.AddListener(OnLeftButtonValueChanged);
        YButton.onValueChanged.AddListener(OnLeftButtonValueChanged);

        useLeftStick.onValueChanged.AddListener(OnUseLeftStickValueChanged);
        useRightStick.onValueChanged.AddListener(OnUseRightStickValueChanged);
        useDPad.onValueChanged.AddListener(OnUseDPadValueChanged);

        SetControllerConfiguration(0);
    }

    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    private void GeneratActionsList()
    {
        List<string> options = Enum.GetNames(typeof(CharacterAction)).ToList();
        
        leftButton.ClearOptions();  leftButton.AddOptions(options);
        rightButton.ClearOptions(); rightButton.AddOptions(options);

        AButton.ClearOptions(); AButton.AddOptions(options);
        BButton.ClearOptions(); BButton.AddOptions(options);
        XButton.ClearOptions(); XButton.AddOptions(options);
        YButton.ClearOptions(); YButton.AddOptions(options);
    }

    #region Current player navigation
    public void SetControllerConfiguration(int index)
    {
        currentPlayer = Mathf.Clamp(index, 0, InputHandler.Instance.ConnectedPlayersCount - 1);
        playerNumber.text = "Player " + (index + 1);

        playerNext.interactable = index < InputHandler.Instance.ConnectedPlayersCount;
        playerPrevious.interactable = currentPlayer >= 0;

        LoadPlayerConfiguration(index);
    }
    
    public void NextPlayer()
    {
        SetControllerConfiguration(currentPlayer + 1);
    }

    public void PreviousPlayer()
    {
        SetControllerConfiguration(currentPlayer - 1);
    }
    
    private void LoadPlayerConfiguration(int index)
    {
        // Load From persistance
        /*
        useLeftStick.isOn = ;
        useRightStick.isOn = ;
        useDPad.isOn = ;

        leftButton.value = ;
        rightButton.value = ;
        AButton.value = ;
        BButton.value = ;
        XButton.value = ;
        YButton.value = ;
        */
    }
    #endregion

    #region Customize actions buttons
    public void OnLeftButtonValueChanged(int index)
    {
        InputHandler.Instance.UpdateGamepadButton(index, GamepadButton.Left, (CharacterAction)Enum.Parse(typeof(CharacterAction), leftButton.options[index].text));
    }

    public void OnRightButtonValueChanged(int index)
    {
        InputHandler.Instance.UpdateGamepadButton(index, GamepadButton.Right, (CharacterAction)Enum.Parse(typeof(CharacterAction), rightButton.options[index].text));
    }

    public void OnAtButtonValueChanged(int index)
    {
        InputHandler.Instance.UpdateGamepadButton(index, GamepadButton.A, (CharacterAction)Enum.Parse(typeof(CharacterAction), AButton.options[index].text));
    }

    public void OnBButtonValueChanged(int index)
    {
        InputHandler.Instance.UpdateGamepadButton(index, GamepadButton.B, (CharacterAction)Enum.Parse(typeof(CharacterAction), BButton.options[index].text));
    }
    public void OnXtButtonValueChanged(int index)
    {
        InputHandler.Instance.UpdateGamepadButton(index, GamepadButton.X, (CharacterAction)Enum.Parse(typeof(CharacterAction), XButton.options[index].text));
    }

    public void OnYButtonValueChanged(int index)
    {
        InputHandler.Instance.UpdateGamepadButton(index, GamepadButton.Y, (CharacterAction)Enum.Parse(typeof(CharacterAction), YButton.options[index].text));
    }
    #endregion

    #region Customize movement axis
    public void OnUseLeftStickValueChanged(bool value)
    {
        InputHandler.Instance.SetActiveLeftStick(currentPlayer, value);
    }

    public void OnUseRightStickValueChanged(bool value)
    {
        InputHandler.Instance.SetActiveRightStick(currentPlayer, value);
    }

    public void OnUseDPadValueChanged(bool value)
    {
        InputHandler.Instance.SetActiveDPad(currentPlayer, value);
    }
    #endregion

    public void ReturnToPreviousScreen()
    {
        //Save to persistance all players 
        SlideManager.Instance.GoBack();
    }

    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
