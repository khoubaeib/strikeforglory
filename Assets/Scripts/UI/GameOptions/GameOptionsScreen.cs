﻿using Rewired;
using UnityEngine;

public class GameOptionsScreen : SlideSingleton<GameOptionsScreen>
{
    private bool back = false;

    private void Start()
    {
        Slide.OnAfterFadeOut += (() => back = true);
        Slide.OnBeforeFadeIn += (() => back = false);
    }
    private void OnDestroy()
    {
        Slide.OnAfterFadeOut = null;
        Slide.OnBeforeFadeIn = null;
    }

    public void GeneralOptions()
    {
        Debug.Log("General pressed !");
        SlideManager.Instance.Switch(GeneralOptionsScreen.Slide);
    }

    public void ControlsOptions()
    {
        Debug.Log("Control pressed !");
        SlideManager.Instance.Switch(InputOptionsScreen.Slide);
    }

    public void VideoOptions()
    {
        SlideManager.Instance.Switch(VideoOptionsScreen.Slide);
    }

    public void AudioOptions()
    {
        Debug.Log("Audio pressed !");
        SlideManager.Instance.Switch(AudioOptionsScreen.Slide);
    }

    public void ReturnToPreviousScreen()
    {
        SlideManager.Instance.GoBack();
    }
    
    private void Update()
    {
        if (!back)
            return;

        foreach (var p in ReInput.players.Players)
        {
            if (p.GetButtonDown("UICancel"))
                ReturnToPreviousScreen();
        }
    }
}
