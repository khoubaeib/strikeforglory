﻿using UnityEngine;

public class VideoSettingsData : Data<float, Resolution, bool , bool> , IPersistantSettings
{
    public float Brightness
    {
        get { return value0; }
        set { value0 = value; }
    }

    public Resolution ScreenResolution
    {
        get { return value1; }
        set { value1 = value; }
    }
    
    public bool FullScreen
    {
        get { return value2; }
        set { value2 = value; }
    }

    public bool VSync
    {
        get { return value3; }
        set { value3 = value; }
    }


    public VideoSettingsData(bool readPersistantData = true) : 
        base("VIDEO_DATA", 1f, new Resolution { width = Screen.width, height = Screen.height, refreshRate = Screen.currentResolution.refreshRate, },  true, false)
    {
        if (readPersistantData)
            ReadData();
    }

    public void ReadData()
    {
        VideoSettingsData data = PersistenceManager.GetData<VideoSettingsData>(id);
        if (data == null)
            return;

        Brightness = data.Brightness;
        ScreenResolution = data.ScreenResolution;
        FullScreen = data.FullScreen;
        VSync = data.VSync;
    }

    public void SaveData()
    {
        PersistenceManager.UpdateData(this);
    }
}
