﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPersistantSettings
{
    void ReadData();

    void SaveData();
}
