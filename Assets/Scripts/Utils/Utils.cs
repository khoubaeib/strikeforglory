﻿using UnityEngine;
using UnityEngine.UI;

public static class Utils
{
    public static bool AreFacing(MoveComponent each, MoveComponent other)
    {
        return each.Direction != other.Direction && IsInFrontOf(each, other.transform);
    }

    public static bool IsInFrontOf(MoveComponent each, Transform other)
    {
        return Vector3.Dot(other.position - each.transform.position, each.transform.right * each.Direction) > 0;
    }

    public static Vector2 Snap(Vector2 direction, int directionsCount)
    {
        
        float angle = Vector2.SignedAngle(direction, Vector2.up);
        if (Mathf.Approximately(angle, 0)) return Vector2.up;
        if (Mathf.Approximately(angle, 90))  return Vector2.right;
        if (Mathf.Approximately(angle, 180)) return Vector2.down;
        if (Mathf.Approximately(angle, -90)) return Vector2.left;
        return Vector2.up;


        /*
        float angle = Vector2.SignedAngle(direction, Vector2.up);
        angle = Mathf.Sign(angle) > 0f ? angle : Mathf.Lerp(180f, 360f, Mathf.InverseLerp(180f, 0f, Mathf.Sign(angle)));


        float snapped = 360f / (float)directionsCount;



        if (Mathf.Approximately(angle, 0)) return Vector2.up;
        if (Mathf.Approximately(angle, 90)) return Vector2.right;
        if (Mathf.Approximately(angle, 180)) return Vector2.down;
        if (Mathf.Approximately(angle, -90)) return Vector2.left;*/
    }

    /*
    public static void SetSubTransformState(this Transform transform, bool active)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            SetRecursiveState(transform.GetChild(i), active);
        }
    }

    public static void SetRecursiveState(this Transform transform, bool active)
    {
        transform.gameObject.SetActive(active);

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(active);
            SetRecursiveState(transform.GetChild(i), active);
        }
    }

    public static void SetRecursiveInteractible(this Transform transform, bool active)
    {
        Selectable s = transform.GetComponent<Selectable>();
        if(s) s.interactable = active;

        for (int i = 0; i < transform.childCount; i++)
        {
            s = transform.GetChild(i).GetComponent<Selectable>();
            if (s) s.interactable = active;

            SetRecursiveInteractible(transform.GetChild(i), active);
        }
    }

    public static Selectable GetFirstSelectable(Transform transform, int x = 0)
    {
        Selectable s = transform.GetComponent<Selectable>();
        if (s && s.interactable)
            return s;
        
        for (int i = 0; i < transform.childCount; i++)
        {
            s = GetFirstSelectable(transform.GetChild(i), x + 1);
            if (s && s.interactable)
                return s;
        }

        return null;
    }*/
}
