﻿using UnityEngine;

public class EndSplashScreen : MonoBehaviour
{
    public void DoEndSplashScreen()
    {
        SplashScreenManager.Instance.GotoMainMenu();
    }
}
