﻿using UnityEngine;

public class SplashScreenManager : Singleton<SplashScreenManager>
{
    [SerializeField] private float minSkipTime = 0.5f;
    
    private float coolDown = 0f;
    private bool mainMenuLoaded = false;

    void Start ()
    {
        coolDown = Time.time;
    }

    private void Update()
    {        
        if (Input.anyKeyDown)
            GotoMainMenu();
    }

    public void GotoMainMenu()
    {
        if ((Time.time - coolDown) < minSkipTime)
            return;

        if (mainMenuLoaded)
            return;

        mainMenuLoaded = true;
        SlideManager.Instance.SwitchMainMenu<StartScreen>();
    }
}
