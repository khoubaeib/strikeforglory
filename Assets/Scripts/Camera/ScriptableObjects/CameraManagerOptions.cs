﻿using UnityEngine;

[CreateAssetMenu(menuName = "Options/CameraManagerOptions")]
public class CameraManagerOptions : ScriptableObject
{
    [Header("Follow")]
    public Vector3 Offset;
    public float FollowSpeed;
    public Vector2 DeadZoneRange;

    [Header("Distance")]
    public float minHorizontalDistance;
    public float maxHorizontalDistance;
    public float minVerticalDistance;
    public float maxVerticalDistance;

    [Header("Zoom")]
    public float minZoom;
    public float maxZoom;
    public float zoomSpeed;
    public AnimationCurve horizontalRatio;
    public AnimationCurve verticalRatio;

    [Header("Shake")]
    public bool canShake;
    public float shakeIntensity;
    public float shakeTime;
    public float shakeStep;

    [Header("Focus")]
    public bool canFocus;
    public float FocusSpeed;
    [Range(0f, 1f)] public float ZoomRatio;
    public float FocusDuration;

    [Header("Debug")]
    public bool debug;
}
