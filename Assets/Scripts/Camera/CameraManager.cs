﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum CameraMode
{
    Game,
    Action
}


[RequireComponent(typeof(Camera))]
public class CameraManager : Singleton<CameraManager>
{
    #region Inspector
    [SerializeField] private CameraManagerOptions options;
    #endregion

    #region fields
    private CameraMode mode = CameraMode.Game;

    private List<Transform> targets = new List<Transform>();

    private Vector3 shakeOffset;
    private Coroutine shakeRoutine;

    private float focusZoom;
    private Vector3 focusPosition;
    private Coroutine focusRoutine;
    #endregion

    #region Components
    private new Camera camera;
    #endregion

    #region Unity Routine
    private void Start()
    {
        camera = GetComponent<Camera>();

    }

    private void Update()
    {
        UpdateCamera();
    }
    #endregion

    #region Logic
    private void UpdateCamera()
    {
        switch(mode)
        {
            case CameraMode.Game: GameCameraZoom(); GameCameraMove(); break;
            case CameraMode.Action: ActionCameraZoom(); ActionCameraMove(); break;
        }
    }

    #region Game
    private void GameCameraZoom()
    {
        float hDistance = Mathf.InverseLerp(options.minHorizontalDistance, options.maxHorizontalDistance, GetHorizontalDistance());
        float hZoom = Mathf.Lerp(options.minZoom, options.maxZoom, Mathf.Clamp01(options.horizontalRatio.Evaluate(hDistance)));
        float vDistance = Mathf.InverseLerp(options.minVerticalDistance, options.maxVerticalDistance, GetVerticalDistance());
        float vZoom = Mathf.Lerp(options.minZoom, options.minZoom, Mathf.Clamp01(options.verticalRatio.Evaluate(vDistance)));
        camera.orthographicSize = Mathf.MoveTowards(camera.orthographicSize, Math.Max(hZoom, vZoom), options.zoomSpeed * Time.deltaTime);
    }

    private void GameCameraMove()
    {
        transform.position = Vector3.MoveTowards(transform.position, FilterDeadZoneArea(GetAnchorPoint() + options.Offset + shakeOffset), options.FollowSpeed * Time.deltaTime);
    }

    private Vector3 FilterDeadZoneArea(Vector3 position)
    {
        Bounds deadZone = new Bounds(transform.position, options.DeadZoneRange);
        if (deadZone.Contains(position))
            return transform.position;
        
        return position;
    }
    #endregion

    #region Focus Mode
    private void ActionCameraZoom()
    {
        camera.orthographicSize = Mathf.MoveTowards(camera.orthographicSize, focusZoom, options.zoomSpeed * Time.deltaTime);
    }

    private void ActionCameraMove()
    {
        transform.position = Vector3.MoveTowards(transform.position, focusPosition + shakeOffset, options.FocusSpeed);
    }
    #endregion

    #endregion

    #region Targets
    public void CenterCamera()
    {
        transform.position = GetAnchorPoint() + options.Offset + shakeOffset;
    }

    public void AddTarget(Transform t)
    {
        if (targets.Contains(t))
            return;

        targets.Add(t);
    }
    
    public void RemoveTarget(Transform t)
    {
        if (!targets.Contains(t))
            return;

        targets.Remove(t);
    }

    public float GetHorizontalDistance()
    {
        float distance = 0f;
        foreach(var t1 in targets)
            foreach (var t2 in targets)
            {
                if (t1 == t2)
                    continue;

                distance = Math.Max(distance, Mathf.Abs(t1.position.x - t2.position.x));
            }

        return distance;
    }

    public float GetVerticalDistance()
    {
        float distance = 0f;
        foreach (var t1 in targets)
            foreach (var t2 in targets)
            {
                if (t1 == t2)
                    continue;

                distance = Math.Max(distance, Mathf.Abs(t1.position.y - t2.position.y));
            }

        return distance;
    }

    public Vector3 GetAnchorPoint()
    {
        if (targets.Count == 0) return Vector3.zero;
        if (targets.Count == 1) return targets[0].position;

        var bounds = new Bounds(targets[0].transform.position, Vector3.zero);
        for (var i = 1; i < targets.Count; i++)
            bounds.Encapsulate(targets[i].transform.position);

        return bounds.center;
    }
    #endregion

    #region Shake
    public void Shake()
    {
        Shake(options.shakeIntensity, options.shakeTime, options.shakeStep);
    }

    public void Shake(float intensity, float duration, float step)
    {
        if (!options.canShake)
            return;

        if(shakeRoutine != null)
        {
            StopCoroutine(shakeRoutine);
            shakeRoutine = null;
        }

        shakeRoutine = StartCoroutine(ShakeRoutine(duration, intensity, step));
    }

    public IEnumerator ShakeRoutine(float duration, float intensity, float step)
    {
        float end = Time.time + duration;
        while (end > Time.time)
        {
            shakeOffset = UnityEngine.Random.insideUnitSphere * intensity;
            yield return new WaitForSeconds(step);
        }

        shakeOffset = Vector3.zero;
    }
    #endregion

    
    #region Focus
    public void FocusAction(Vector3 position)
    {
        FocusAction(position, options.ZoomRatio, options.FocusDuration);
    }

    public void FocusAction(Vector3 position, float zoomRatio, float duration)
    {
        if (!options.canFocus)
            return;

        if (focusRoutine != null)
        {
            StopCoroutine(focusRoutine);
            focusRoutine = null;
        }

        focusRoutine = StartCoroutine(FocusRoutine(position, zoomRatio, duration));
    }

    private IEnumerator FocusRoutine(Vector3 position, float zoomRatio, float duration)
    {
        focusPosition = position;
        focusZoom = Mathf.InverseLerp(options.maxZoom, options.minZoom, Mathf.Clamp01(zoomRatio));
        mode = CameraMode.Action;
        yield return new WaitForSeconds(duration);
        mode = CameraMode.Game;
    }
    #endregion

    #region Editor
    private void OnDrawGizmos()
    {
        if (!options.debug)
            return;

        Gizmos.DrawWireSphere(GetAnchorPoint() + options.Offset, 10f);
    }

    private void OnGUI()
    {
        if (!options.debug)
            return;

        GUILayout.Label("-- Camera --");
        GUILayout.Label("Horizontal Distance ==> " + GetHorizontalDistance());
        GUILayout.Label("Vertical Distance ==> " + GetVerticalDistance());
        GUILayout.Label("Zoom ==> " + camera.orthographicSize);
    }
    #endregion
}
