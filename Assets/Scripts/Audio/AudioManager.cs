﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : PersistentSingleton<AudioManager>
{

    [SerializeField] private AudioMixer mixer;

    [SerializeField] private string MasterLabel;
    [SerializeField] private string MusicGroupLabel;
    [SerializeField] private string EffectsGroupLabel;
    
    [SerializeField] private AudioClip mainMenuTheme;
    [SerializeField] private AudioClip GameTheme;

    private new AudioSource audio;
    private AudioSource GetAudioSource()
    {
        return audio ? audio : audio = GetComponent<AudioSource>();
    }

    public void ApplySettings(AudioSettingsData audioData)
    {
        SetMasterVolume(audioData.Master);
        SetEffectsVolume(audioData.Effects);
        SetMusicAudio(audioData.Music);
    }

    public void SetMasterVolume(float value)
    {
        mixer.SetFloat(MasterLabel, value);
    }

    public void SetMusicAudio(float value)
    {
        mixer.SetFloat(MusicGroupLabel, value);
    }

    public void SetEffectsVolume(float value)
    {
        mixer.SetFloat(EffectsGroupLabel, value);
    }
    
    public void PlayMainMenuTheme()
    {
        GetAudioSource().Stop();
        GetAudioSource().clip = mainMenuTheme;
        GetAudioSource().Play();

    }

    public void PlayGameTheme()
    {
        GetAudioSource().Stop();
        GetAudioSource().clip = GameTheme;
        GetAudioSource().Play();
    }

    public void PauseMusic()
    {
        GetAudioSource().Pause();
    }

    public void ResumeMusic()
    {
        GetAudioSource().Play();
    }


}