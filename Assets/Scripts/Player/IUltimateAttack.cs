﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUltimateAttack
{
    bool CanAttack
    {
        get;
    }
    
    void Attack();
}
