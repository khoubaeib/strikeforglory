﻿using System;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(PhysicComponent))]
[RequireComponent(typeof(AttackComponent))]
public class MoveComponent : MonoBehaviour, IPlayerComponent
{
    #region Inspector 
    [SerializeField] private MoveComponentOptions options;
    #endregion

    #region Components accessors
    private PhysicComponent physic;
    private PhysicComponent GetPhysic()
    {
        return physic ? physic : physic = GetComponent<PhysicComponent>();
    }

    private AttackComponent attack;
    private AttackComponent GetAttack()
    {
        return attack ? attack : attack = GetComponent<AttackComponent>();
    }

    private new SpriteRenderer renderer;
    private SpriteRenderer GetRenderer()
    {
        return renderer ? renderer : renderer = GetComponent<SpriteRenderer>();
    }
    #endregion

    #region Fields
    private float direction = 1f;
    private float jumpTimer = 0f;
    private float dashTimer = 0f;
    private float nextDashTime = 0f;
    private float dashDistanceRatio = 0f;
    private bool airDash = false;
    private bool doubleJump = false;
    private bool isRunning = false;

    private bool behaviour = true;
    #endregion

    #region Properties
    public float Direction
    {
        get { return direction; }
    }
    #endregion

    #region Unity routines
    private void Start() 
    {
        GetPhysic().OnLand += OnGround;
    }

    private void OnDestroy()
    {
        GetPhysic().OnLand -= OnGround;
    }

    private void FixedUpdate()
    {
        JumpRoutine();
    }
    #endregion

    #region Action routines
    private void JumpRoutine()
    {
        if (!behaviour)
            return;

        if (jumpTimer <= 0)
            return;
        
        physic.Jump(options.jumpForceOverTime.Evaluate(Mathf.InverseLerp(options.maxJumpTime, 0f, jumpTimer)));
        jumpTimer -= Time.fixedDeltaTime;
    }
    
    private void OnGround()
    {
        airDash = true;
    }
    #endregion

    #region Behaviour access
    public void Move(float direction)
    {
        if (!behaviour)
            return;

        if (GetAttack().IsEngaged)
        {
            UpdateDirection(direction);
            return;
        }

        bool groundMove = options.canMove && GetPhysic().IsGrounded;
        bool airMove = options.canMove && !GetPhysic().IsGrounded && options.canMoveInAir;
        if (!groundMove && !airMove)
            return;

        GetPhysic().Move(direction * (isRunning ? options.runSpeed : options.moveSpeed));
        UpdateDirection(direction);
    }

    public void FallDown()
    {
        if (!behaviour)
            return;

        if (!options.canFallDown || GetPhysic().IsGrounded)
            return;

        jumpTimer = 0f;
        GetPhysic().FallDown(options.fallDownForce);
        doubleJump = false;
    }

    public void MoveDown()
    {
        if (!behaviour)
            return;

        if (!GetPhysic().IsGrounded)
            return;

        GetPhysic().MoveDown();
    }
    
    private void UpdateDirection(float newDirection)
    {
        direction = newDirection == 0 ? direction : Math.Sign(newDirection);
        GetRenderer().flipX = direction == 1;
    }

    public void StartJump()
    {
        if (!behaviour)
            return;

        FirstJump();
        SecondJump();
    }

    private void FirstJump()
    {
        if (!options.canJump || !GetPhysic().IsGrounded || !options.dynamicJump)
            return;

        if (jumpTimer > 0f)
            return;

        jumpTimer = options.maxJumpTime;
        doubleJump = true;
    }

    private void SecondJump()
    {
        if (!options.canJump || GetPhysic().IsGrounded || !options.canDoubleJump || !doubleJump)
            return;

        jumpTimer = 0f;
        GetPhysic().Jump(options.doubleJumpForce);
        doubleJump = false;
    }

    public void StopJump()
    {
        if (!behaviour)
            return;

        if (!options.dynamicJump)
            return;

        jumpTimer = 0f;
    }

    public void StartDash()
    {
        if (!behaviour)
            return;

        if (GetAttack().IsEngaged)
            return;

        if (!options.canDash || isRunning || nextDashTime > Time.time)
            return;

        if (!GetPhysic().IsGrounded && !airDash)
            return;

        dashDistanceRatio = Mathf.Clamp01(dashDistanceRatio + Mathf.InverseLerp(0f, options.dashFullCoolDown, Time.time - dashTimer));
        if (Mathf.Approximately(dashDistanceRatio, 0f))
            return;

        GetPhysic().StartDash(direction, options.dashSpeed, options.dashDistance * dashDistanceRatio);
        dashDistanceRatio = Mathf.Clamp(dashDistanceRatio - options.distanceLossPerDash, options.MinDistanceRatio, 1f);
        dashTimer = Time.time + options.dashFullCoolDown;

        nextDashTime = Time.time + options.dashInterval;

        isRunning = true;
    }

    public void StopDash()
    {
        if (!behaviour)
            return;

        if (!physic.IsGrounded && !airDash)
            return;

        airDash = GetPhysic().IsGrounded;
        GetPhysic().StopDash();
        isRunning = false;
    }
    #endregion
    
    #region Pause/Resume
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }
    #endregion

    #region Debug
#if UNITY_EDITOR

    private void OnGUI()
    {
        if (!options.debug)
            return;
    }


#endif
    #endregion
}
