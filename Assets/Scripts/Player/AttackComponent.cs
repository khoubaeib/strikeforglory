﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PhysicComponent))]
[RequireComponent(typeof(MoveComponent))]
public class AttackComponent : MonoBehaviour, IPlayerComponent
{
    #region Event
    public Action OnHold;
    public Action OnBeginAttack;
    public Action OnEndAttack;
    public Action OnBeginVunerable;
    public Action OnEndVunerable;
    #endregion

    #region Inspector 
    [SerializeField] private AttackComponentOptions options;
    #endregion

    #region Components
    private MoveComponent move;
    private MoveComponent GetMove()
    {
        return move ? move : move = GetComponent<MoveComponent>();
    }

    private PhysicComponent physic;
    private PhysicComponent GetPhysic()
    {
        return physic ? physic : physic = GetComponent<PhysicComponent>();
    }

    private new Rigidbody2D rigidbody;
    private Rigidbody2D GetRigidbody()
    {
        return rigidbody ? rigidbody : rigidbody = GetComponent<Rigidbody2D>();
    }
    #endregion

    #region Fields
    private bool isHolding = false;
    private bool isAttacking = false;
    private bool isVunerable = false;

    private float attackCoolDown = 0f;
    private Vector2 direction = Vector2.zero;

    private bool behaviour = true;
    #endregion
    
    public Vector2 Direction
    {
        get { return direction; }
    }
    
    public bool IsEngaged
    {
        get { return isHolding || isAttacking || isVunerable; }
    }


    private void Update()
    {
        DoAttack();
    }
    
    public void SetDirection(Vector2 direction)
    {
        if (!behaviour)
            return;

        if (isAttacking)
            return;

        this.direction = direction == Vector2.zero ? Vector2.right * GetMove().Direction : Snap(direction.normalized).normalized;
    }

    public void Hold()
    {
        if (!behaviour)
            return;

        //!GetPhysic().IsStatic || 
        if (GetPhysic().IsDashing)
            return;

        if (!options.canAttack || isHolding || isAttacking)
            return;

        if (attackCoolDown > Time.time)
            return;
        
        isHolding = true;
        
        if (OnHold != null)
            OnHold();
    }

    // TO DO pause mechaninc
    public void Attack()
    {
        if (!behaviour)
            return;

        if (!options.canAttack || !isHolding || isAttacking)
            return;
        
        isHolding = false;
        isAttacking = true;
        
        if (OnBeginAttack != null)
            OnBeginAttack();

        attackCoolDown = Time.time + options.attackInterval;
    }
    
    public void EndAttack()
    {
        if (!behaviour)
            return;

        if (!options.canAttack || !isAttacking)
            return;

        isAttacking = false;
        
        if (OnEndAttack != null)
            OnEndAttack();
        
        BeginVunerable();
    }

    public void BeginVunerable()
    {
        if (!behaviour)
            return;

        if (!options.canAttack || isVunerable)
            return;

        isVunerable = true;

        if (OnBeginVunerable != null)
            OnBeginVunerable();
    }

    public void EndVunerable()
    {
        if (!behaviour)
            return;

        if (!options.canAttack || !isVunerable)
            return;

        isVunerable = false;

        if (OnEndVunerable != null)
            OnEndVunerable();
    }

    /*private IEnumerator DoAttack(Vector2 attackDirection) 
    {
        if (OnAttack != null)
            OnAttack();

        isAttacking = true;

        float attackSyncDelay = options.attackSyncDelay;
        while (attackSyncDelay > 0)
        {
            UpdateAttackDirection(attackDirection);
            yield return new WaitForEndOfFrame();
            attackSyncDelay -= Time.deltaTime;
        }
        
        if (OnBeginAttack != null)
            OnBeginAttack();

        float attackDuration = options.attackDuration;
        while (attackDuration > 0)
        {
            UpdateAttackDirection(attackDirection);
            Collider2D[] opponents = Physics2D.OverlapCircleAll(position, options.hitZoneRadius, options.playersLayer);
            foreach (var o in opponents)
            {
                if (Ignore(o)) continue;
                if (Clash(o))
                {
                    attackDuration = 0;
                    break;
                }

                Kill(o);
            }

            yield return new WaitForEndOfFrame();
            attackDuration -= Time.deltaTime;
        }

        isAttacking = false;
        
        attackMarker.transform.position = Vector3.zero;
        attackMarker.transform.localScale = Vector3.zero;

        if (OnFinishAttack != null)
            OnFinishAttack();
    }*/

    private void DoAttack()
    {
        if (!isAttacking)
            return;
        
        Vector3 position = GetRigidbody().position + options.hitZoneOffset + (direction * options.hitZoneDistance);
        Collider2D[] opponents = Physics2D.OverlapCircleAll(position, options.hitZoneRadius, options.playersLayer);
        foreach (var o in opponents)
        {
            if (o.GetComponent<PlayerComponent>().IsDead)
                return;

            if (Ignore(o)) continue;
            if (Clash(o)) return;
            if (Kill(o)) return;
                
        }
    }

    private bool Ignore(Collider2D opponent)
    {
        return opponent.transform == transform || opponent.GetComponent<PhysicComponent>().IsDashing;
    }

    private bool Clash(Collider2D opponent)
    {
        var other = opponent.GetComponent<AttackComponent>();
        if (!options.onlyPushBack && !((other.isHolding || other.isAttacking) && Utils.AreFacing(GetMove(), opponent.GetComponent<MoveComponent>())))
            return false;
        
        PushBack(opponent.transform.position);
        other.PushBack(transform.position);
        return true;
    }

    private bool Kill(Collider2D opponent)
    {
        if (options.onlyPushBack || !Utils.IsInFrontOf(GetMove(), opponent.transform) || opponent.GetComponent<AttackComponent>().isAttacking)
            return false;

        Vector3 to = ((opponent.transform.position - transform.position).normalized + Vector3.up).normalized;

        opponent.GetComponent<PhysicComponent>().Push(1, new Vector2(to.x * 30, to.y));
        opponent.GetComponent<PlayerComponent>().Die(GetComponent<PlayerComponent>());
        return true;
    }

    private void PushBack(Vector3 opponent)
    {
        if(!behaviour)
            return;
        
        if (!options.pushBack)
            return;
        
        GetPhysic().Push(options.pushBackForce, (transform.position - opponent).normalized);
    }

    private Vector2 Snap(Vector2 direction)
    {
        float angle = Vector2.SignedAngle(direction, Vector2.up);
        //Debug.Log("angle => " + angle);
        if (Mathf.Approximately(angle, 0))   return Vector2.up;
        if (Mathf.Approximately(angle, 90))  return Vector2.right;
        if (Mathf.Approximately(angle, 180)) return Vector2.down;
        if (Mathf.Approximately(angle, -90)) return Vector2.left;
        
        return Vector2.up;


        //direction.x = direction.x > options.axisTolerance ? 1f : direction.x < -options.axisTolerance ? -1f : 0f;
        //direction.y = direction.y > options.axisTolerance ? 1f : direction.y < -options.axisTolerance ? -1f : 0f;
        //return direction.normalized;
    }

    #region Pause/Resume
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }
    #endregion

    #region Debug
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!options.debug)
            return;

        Color c = Gizmos.color;
        Gizmos.color = isAttacking ? Color.red : Color.yellow;
        Gizmos.DrawSphere(GetRigidbody().position + options.hitZoneOffset + (direction * options.hitZoneDistance), options.hitZoneRadius);
        Gizmos.color = c;
    }

    
    private void OnGUI()
    {
        if (!options.debug)
            return;

        GUILayout.Label("-- Attack Component --");
        GUILayout.Label("Direction => " + direction);
    }
    

#endif
    #endregion
}