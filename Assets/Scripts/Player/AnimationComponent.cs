﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PhysicComponent))]
[RequireComponent(typeof(AttackComponent))]
[RequireComponent(typeof(PlayerComponent))]
public class AnimationComponent : MonoBehaviour, IPlayerComponent
{

    #region Inspector
    [SerializeField] private string deadLabel = "Dead";
    [SerializeField] private string stillLabel = "Still";
    [SerializeField] private string groundedLabel = "Grounded";
    [SerializeField] private string velocityLabel = "Velocity";
    [SerializeField] private string speedLabel = "Speed";
    [SerializeField] private string dashLabel = "Dash";
    [SerializeField] private string holdLabel = "Hold";
    [SerializeField] private string attackLabel = "Attack";
    [SerializeField] private string directionLabel = "Direction";
    [SerializeField] private string vunerableLabel = "Vunerable";
    #endregion

    #region Components
    private Animator animator;
    private Animator GetAnimator()
    {
        return animator ? animator : animator = GetComponent<Animator>();
    }

    private PhysicComponent physic;
    private PhysicComponent GetPhysic()
    {
        return physic ? physic : physic = GetComponent<PhysicComponent>();
    }

    private AttackComponent attack;
    private AttackComponent GetAttack()
    {
        return attack ? attack : attack = GetComponent<AttackComponent>();
    }

    private PlayerComponent player;
    private PlayerComponent GetPlayer()
    {
        return player ? player : player = GetComponent<PlayerComponent>();
    }
    #endregion

    private bool behaviour = true;

    void Start ()
    {
        GetPhysic().OnStartDash += OnPlayerStartDash;
        GetPhysic().OnStopDash += OnPlayerStopDash;

        GetAttack().OnHold += OnHold;
        GetAttack().OnBeginAttack += OnBeginAttack;
        GetAttack().OnEndAttack += OnFinishAttack;
        GetAttack().OnBeginVunerable += OnBeginVunerable;
        GetAttack().OnEndVunerable += OnEndVunerable;

        GetPlayer().OnDead += OnPlayerDead;
    }

    private void OnDestroy()
    {
        GetPhysic().OnStartDash -= OnPlayerStartDash;
        GetPhysic().OnStopDash -= OnPlayerStopDash;

        GetAttack().OnHold -= OnHold;
        GetAttack().OnBeginAttack -= OnBeginAttack;
        GetAttack().OnEndAttack -= OnFinishAttack;
        GetAttack().OnBeginVunerable -= OnBeginVunerable;
        GetAttack().OnEndVunerable -= OnEndVunerable;

        GetPlayer().OnDead -= OnPlayerDead;
    }

    #region Dead
    private void OnPlayerDead()
    {
        GetAnimator().SetBool(deadLabel, true);
    }
    #endregion

    #region movement
    private void Update()
    {
        UpdateSpeed();
        UpdateVelocity();
        UpdateDirection();
    }
    
    private void UpdateVelocity()
    {
        GetAnimator().SetBool(groundedLabel, GetPhysic().IsGrounded);
        GetAnimator().SetFloat(velocityLabel, GetPhysic().Velocity.y);
    }

    private void UpdateSpeed()
    {
        GetAnimator().SetBool(stillLabel, GetPhysic().IsStatic);
        GetAnimator().SetFloat(speedLabel, Mathf.Abs(GetPhysic().Speed));
    }

    private void UpdateDirection()
    {
        GetAnimator().SetInteger(directionLabel, GetAttack().Direction == Vector2.up ? 1 : GetAttack().Direction == Vector2.down ? 2 : 0);
    }
    #endregion

    #region Attack
    private void OnHold()
    {
        if (!behaviour)
            return;

        GetAnimator().SetBool(holdLabel, true);
    }

    private void OnBeginAttack()
    {
        if (!behaviour)
            return;

        GetAnimator().SetBool(holdLabel, false);
        GetAnimator().SetBool(attackLabel, true);
    }

    private void OnFinishAttack()
    {
        if (!behaviour)
            return;

        GetAnimator().SetBool(attackLabel, false);
    }

    private void OnBeginVunerable()
    {
        if (!behaviour)
            return;

        GetAnimator().SetBool(vunerableLabel, true);
    }

    private void OnEndVunerable()
    {
        if (!behaviour)
            return;

        GetAnimator().SetBool(vunerableLabel, false);
    }
    #endregion

    #region Dash
    private void OnPlayerStartDash()
    {
        GetAnimator().SetBool(dashLabel, true);
    }

    private void OnPlayerStopDash()
    {
        GetAnimator().SetBool(dashLabel, false);
    }
    #endregion

    #region Pause/Resume
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
        GetAnimator().enabled = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        GetAnimator().enabled = true;
        behaviour = true;
    }
    #endregion
}
