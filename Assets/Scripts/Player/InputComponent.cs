﻿using UnityEngine;

[RequireComponent(typeof(AttackComponent))]
[RequireComponent(typeof(MoveComponent))]
public class InputComponent : MonoBehaviour, IPlayerComponent
{

    #region Components
    private MoveComponent move;
    private MoveComponent GetMove()
    {
        return move ? move : move = GetComponent<MoveComponent>();
    }

    private AttackComponent attack;
    private AttackComponent GetAttack()
    {
        return attack ? attack : attack = GetComponent<AttackComponent>();
    }
    #endregion

    private bool behaviour = true;

    public void ProcessInput( GamepadWrapper gamepad)
    {
        if (!behaviour)
            return;
        
        Vector2 axis = gamepad.GetMovement();
        GetMove().Move(axis.x);
        GetAttack().SetDirection(axis);

        if (gamepad.GetStartJump()) GetMove().StartJump();
        if (gamepad.GetStopJump()) GetMove().StopJump();
        if (gamepad.GetStartDash()) GetMove().StartDash();
        if (gamepad.GetStopDash()) GetMove().StopDash();
        if (gamepad.GetAttackDown()) GetAttack().Hold();
        if (gamepad.GetAttackUp()) GetAttack().Attack();
        if (gamepad.GetFallDown()) GetMove().FallDown();
        
    }

    #region Pause/Resume
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }
    #endregion

}
