﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Options/AttackComponentOptions")]
public class AttackComponentOptions : ScriptableObject
{

    [Header("Attack")]
     public bool canAttack;

    [Header("Direction")]
     public bool useSnappedDirections;
     public float axisTolerance = 1.6f;
     public bool disableDownDirection;//  TODO

    [Header("Timing")]
     public float attackInterval;
     public float attackSyncDelay;
     public float attackDuration;

    [Header("HitZone")]
     public Vector2 hitZoneOffset;
     public float hitZoneRadius;
     public float hitZoneDistance;
     public LayerMask playersLayer;

    [Header("PushBack")]
     public bool pushBack;
     public float pushBackForce;

    [Space]
     public bool debug;
     public bool onlyPushBack;
}
