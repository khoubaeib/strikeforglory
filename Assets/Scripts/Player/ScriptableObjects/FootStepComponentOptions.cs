﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


[Serializable]
public class LayerFootStep
{
    public LayerMask layers;
    public AudioClip[] footSteps;

    public bool ContainsLayer(int ground)
    {
        return layers == (layers | (1 << ground));
    }

    public AudioClip GetRandomEffect()
    {
        return footSteps[Random.Range(0, footSteps.Length - 1)];
    }
}

[CreateAssetMenu(menuName = "Options/FootStepComponentOptions")]
public class FootStepComponentOptions : ScriptableObject
{
    public LayerFootStep[] footSteps;

    public bool IsEmpty
    {
        get { return footSteps.Length == 0; }
    }

    public AudioClip GetEffect(int groundLayer)
    {
        foreach (var fs in footSteps)
        {
            if (!fs.ContainsLayer(groundLayer))
                continue;

            return fs.GetRandomEffect();
        }

        return null;
    }
}
