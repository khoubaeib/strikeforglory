﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Options/PhysicComponentOptions")]
public class PhysicComponentOptions : ScriptableObject
{
    public LayerMask groundLayer;
    public LayerMask collidersLayer;
    public LayerMask obstaclesLayer;
    public LayerMask PlatformLayer;
    public Vector2 groundAnchor;
    public float groundDistance = .2f;
    public float groundAnchorsOffset = .2f;
    public float fallThreshold = .2f;
    public bool adjustGravity = true;
    public float fallCorrection = 2.5f;
    public float lowJumpCorrection = 2f;
    public bool adjustFriction = true;
    public float frictionCorrection = 2f;

    [Space]
    public bool debug;
}
