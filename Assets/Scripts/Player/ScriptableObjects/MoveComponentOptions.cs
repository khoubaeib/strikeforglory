﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Options/MoveComponentOptions")]
public class MoveComponentOptions : ScriptableObject
{
    [Header("Mouvement")]
    public bool canMove;
    public bool canMoveInAir;
    public float moveSpeed;
    public float runSpeed;

    [Header("Jump")]
    public bool canJump;
    //public float jumpInterval = 0f;
    public float contantJumpForce;
    public bool dynamicJump;
    public AnimationCurve jumpForceOverTime;
    public float maxJumpTime;
    public bool canDoubleJump;
    public float doubleJumpForce;
    public bool canFallDown;
    public float fallDownForce;

    [Header("Dash")]
    public bool canDash;
    public float dashInterval = 0.3f;
    public float dashSpeed = .2f;
    public float dashDistance = 50f;
    public float dashFullCoolDown = 0.5f;
    [Range(0f, 1f)] public float distanceLossPerDash = 0.5f;
    [Range(0f, 1f)] public float MinDistanceRatio = 0.2f;

    [Space]
    public bool debug;
}
