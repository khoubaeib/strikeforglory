﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PhysicComponent : MonoBehaviour, IPlayerComponent
{
    #region Events
    public Action OnLand;
    public Action OnJump;
    public Action OnFall;
    public Action OnStartDash;
    public Action OnStopDash;
    #endregion

    #region Inspector
    [SerializeField] private PhysicComponentOptions options;
    #endregion

    #region Properties
    public bool IsStatic
    {
        get { return Mathf.Approximately(moveSpeed, 0f); }
    }

    public bool IsGrounded
    {
        get { return isGrounded; }
    }

    public int GroundLayer
    {
        get { return groundLayer; }
    }

    public bool IsRaising
    {
        get { return !IsGrounded && !IsFalling; }
    }

    public bool IsFalling
    {
        get { return isFalling; }
    }

    public bool IsDashing
    {
        get { return isDashing; }
    }

    public Vector3 Velocity
    {
        get { return rigidbody.velocity; }
    }

    public float Speed
    {
        get { return moveSpeed; }
    }
    #endregion

    #region Components
    private new Rigidbody2D rigidbody;
    private new Collider2D collider;
    #endregion

    #region Fields
    private bool behaviour = true;

    private int groundLayer;
    private bool isGrounded = false;
    private bool isFalling = false;
    private bool isDashing = false;

    private float moveSpeed = 0f;
    private float jumpSpeed = 0f;
    private Vector2 dashEndPoint = Vector2.zero;
    private float dashSpeed = 0f;
    private Vector2 pushVelocity = Vector2.zero;
    #endregion

    #region Unity routines
    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();

        Physics2D.queriesStartInColliders = false;
    }

    private void Update()
    {
        PerformChecks();
    }

    private void FixedUpdate()
    {
        PerformActions();
    }
    #endregion

    #region Checkers
    private void PerformChecks()
    {
        CheckGround();
        CheckAir();
    }

    private void CheckGround()
    {
        Vector2 from = rigidbody.position + options.groundAnchor;
        RaycastHit2D grounded = Physics2D.Raycast(from, Vector3.down, options.groundDistance, options.groundLayer);
        grounded = grounded ? grounded : Physics2D.Raycast(from + (Vector2.right * options.groundAnchorsOffset), Vector2.down, options.groundDistance, options.groundLayer);
        grounded = grounded ? grounded : Physics2D.Raycast(from - (Vector2.right * options.groundAnchorsOffset), Vector2.down, options.groundDistance, options.groundLayer);
        if (grounded && !isGrounded && OnLand != null)
        {
            isFalling = false;
            OnLand();
        }

        isGrounded = grounded;
        isFalling = grounded ? false : isFalling;
        groundLayer = grounded ? grounded.transform.gameObject.layer : 0;
    }

    private void CheckAir()
    {
        if (isGrounded)
            return;

        bool falling = rigidbody.velocity.y < options.fallThreshold;
        if (falling && !isFalling && OnFall != null)
            OnFall();

        isFalling = falling;
    }
    #endregion

    #region Actions
    private void PerformActions()
    {
        if (!behaviour)
            return;

        DoMove();
        DoDash();
        DoJump();
        DoPush();
        CorrectGravity();
        CorrectFriction();
    }

    private void DoDash()
    {
        if (!isDashing)
            return;

        float distance = Mathf.Min(Vector2.Distance(rigidbody.position, dashEndPoint), dashSpeed * Time.fixedDeltaTime);
        rigidbody.MovePosition(Vector2.MoveTowards(rigidbody.position, dashEndPoint, distance));

        moveSpeed = jumpSpeed = 0f;
        isDashing = Vector2.Distance(rigidbody.position, dashEndPoint) - distance > 0;
        isDashing = isDashing && !Physics2D.Linecast(rigidbody.position, Vector2.MoveTowards(rigidbody.position, dashEndPoint, distance), options.obstaclesLayer);

        if (!isDashing)
            StopDash();
    }

    private void DoMove()
    {
        if (IsDashing)
            return;

        if (moveSpeed == 0f)
            return;

        float sign = Mathf.Sign(moveSpeed);
        float distance = Mathf.Abs(moveSpeed) * Time.fixedDeltaTime;

        RaycastHit2D[] hits = new RaycastHit2D[10];
        if (rigidbody.Cast(sign * Vector2.right, hits, distance) > 0)
            foreach (var h in hits)
                if (h && h.transform != transform)
                    distance = Mathf.Min(distance, Mathf.Max(h.distance - 0.1f, 0f));

        rigidbody.position += (Vector2.right * distance * sign);
        moveSpeed = 0f;
    }

    private void DoJump()
    {
        if (jumpSpeed == 0f)
            return;

        rigidbody.velocity = new Vector2(rigidbody.velocity.x, jumpSpeed);

        if (OnJump != null)
            OnJump();

        jumpSpeed = 0f;
    }

    private void CorrectGravity()
    {
        if (!options.adjustGravity || IsGrounded)
            return;

        Vector2 correction = Vector2.up * Physics2D.gravity.y * Time.fixedDeltaTime;
        if (IsRaising) rigidbody.velocity += correction * (options.lowJumpCorrection - 1);
        if (IsFalling) rigidbody.velocity += correction * (options.fallCorrection - 1);
    }

    private void CorrectFriction()
    {
        if (!options.adjustFriction)
            return;

        rigidbody.velocity = new Vector2(Mathf.MoveTowards(rigidbody.velocity.x, 0f, options.frictionCorrection * Time.fixedDeltaTime), rigidbody.velocity.y);
    }

    private void DoPush()
    {
        if (pushVelocity.magnitude == 0f)
            return;

        rigidbody.velocity += pushVelocity;
        
        pushVelocity = Vector2.zero;
    }
    #endregion

    #region Behaviour access
    public void Jump(float force)
    {
        if (!behaviour)
            return;

        jumpSpeed = Mathf.Max(0f, force);
    }

    public void Move(float direction)
    {
        if (!behaviour)
            return;

        moveSpeed = direction;
    }

    public void FallDown(float force)
    {
        if (!behaviour)
            return;

        if (isGrounded)
            return;

        jumpSpeed = Mathf.Min(0f, force);
    }

    public void MoveDown()
    {
        if (!behaviour)
            return;

        if (!isGrounded)
            return;

        RaycastHit2D platform = Physics2D.Raycast(rigidbody.position + options.groundAnchor, Vector3.down, options.groundDistance, options.PlatformLayer);
        if (!platform)
            return;
        
        PlatformEffector2D effector = platform.transform.GetComponent<PlatformEffector2D>();
        FallthroughReseter reseter = platform.transform.gameObject.AddComponent<FallthroughReseter>();
        reseter.StartFall(effector, LayerMask.LayerToName(gameObject.layer));
    }

    public void StartDash(float direction, float speed, float distance)
    {
        if (!behaviour)
            return;

        if (isDashing)
            return;

        isDashing = true;
        dashEndPoint = rigidbody.position + (Vector2.right * Mathf.Sign(direction) * distance);
        dashSpeed = speed;

        moveSpeed = jumpSpeed = 0f;
        rigidbody.constraints = RigidbodyConstraints2D.FreezePositionY;
        rigidbody.freezeRotation = true;
        collider.isTrigger = true;

        if (OnStartDash != null)
            OnStartDash();
    }

    public void StopDash()
    {
        if (!behaviour)
            return;

        isDashing = false;
        rigidbody.velocity = Vector2.zero;
        rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        collider.isTrigger = false;

        if (OnStopDash != null)
            OnStopDash();

        if (OnFall != null)
            OnFall();
    }

    public void Push(float force, Vector2 direction)
    {
        if (!behaviour)
            return;

        pushVelocity = direction.normalized * force;
    }
    #endregion

    #region Pause/Resume
    private RigidbodyConstraints2D state =  RigidbodyConstraints2D.FreezeRotation;

    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;

        if (!rigidbody)
            return;

        state = rigidbody.constraints;
        rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        rigidbody.freezeRotation = true;
    }

    public void Resume()
    {
        if (behaviour)
            return;
        
        behaviour = true;
        rigidbody.constraints = state;
        rigidbody.freezeRotation = true;
    }
    #endregion

    #region Debug
#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        if (!options.debug)
            return;

        Color handlesColor = UnityEditor.Handles.color;
        UnityEditor.Handles.color = Color.yellow;
        Vector3 groundStart = transform.position + (Vector3)options.groundAnchor;
        Vector3 groundEnd = groundStart + (Vector3.down * options.groundDistance);
        UnityEditor.Handles.DrawLine(groundStart, groundEnd);
        UnityEditor.Handles.color = handlesColor;
    }

    private void OnGUI()
    {
        if (!options.debug)
            return;

        GUILayout.Label("-- Physic --");
        GUILayout.Label("Move speed => " + moveSpeed);
        GUILayout.Label("Velocity => " + rigidbody.velocity);
        GUILayout.Label("Grounded => " + IsGrounded);
        GUILayout.Label("Raising  => " + IsRaising);
        GUILayout.Label("Falling  => " + IsFalling);
    }
#endif
    #endregion
}
