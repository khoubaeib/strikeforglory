﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerComponent
{
    void Pause();

    void Resume();
}
