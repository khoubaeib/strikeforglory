﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(PhysicComponent))]
[RequireComponent(typeof(AttackComponent))]
[RequireComponent(typeof(PlayerComponent))]
public class SoundFxComponent : MonoBehaviour, IPlayerComponent
{

    #region Inspector
    [SerializeField] private AudioClip[] DeadFx;
    [SerializeField] private AudioClip[] GroundedFx;
    [SerializeField] private AudioClip[] JumpFx;
    [SerializeField] private AudioClip[] FallFx;
    [SerializeField] private AudioClip[] HoldFx;
    [SerializeField] private AudioClip[] AttackFx;
    [SerializeField] private AudioClip[] DashFx;
    #endregion

    #region Components
    private new AudioSource audio;
    private AudioSource GetAudio()
    {
        return audio ? audio : audio = GetComponent<AudioSource>();
    }
    
    private PhysicComponent physic;
    private PhysicComponent GetPhysic()
    {
        return physic ? physic : physic = GetComponent<PhysicComponent>();
    }

    private PlayerComponent player;
    private PlayerComponent GetPlayer()
    {
        return player ? player : player = GetComponent<PlayerComponent>();
    }

    private AttackComponent attack;
    private AttackComponent GetAttack()
    {
        return attack ? attack : attack = GetComponent<AttackComponent>();
    }
    #endregion

    private bool behaviour = true;

    private void Start ()
    {
        GetPhysic().OnLand += OnGrounded;
        GetPhysic().OnJump += OnJump;
        GetPhysic().OnFall += OnFall;
        GetPhysic().OnStartDash += OnDash;

        GetAttack().OnHold += OnHold;
        GetAttack().OnBeginAttack += OnAttack;

        GetPlayer().OnDead += OnDead;
    }

    private void OnDestroy()
    {
        GetPhysic().OnLand -= OnGrounded;
        GetPhysic().OnJump -= OnJump;
        GetPhysic().OnFall -= OnFall;
        GetPhysic().OnStartDash -= OnDash;

        GetAttack().OnHold -= OnHold;
        GetAttack().OnBeginAttack -= OnAttack;

        GetPlayer().OnDead -= OnDead;
    }

    private void OnGrounded()
    {
        PlayerFX(GroundedFx);
    }

    private void OnJump()
    {
        PlayerFX(JumpFx);
    }

    private void OnFall()
    {
        PlayerFX(FallFx);
    }

    private void OnDash()
    {
        PlayerFX(DashFx);
    }

    private void OnHold()
    {
        PlayerFX(HoldFx, true);
    }

    private void OnAttack()
    {
        PlayerFX(AttackFx);
    }
    
    private void OnDead()
    {
        PlayerFX(DeadFx);
    }

    private void PlayerFX(AudioClip soundFx, bool loop = false)
    {
        if (!behaviour)
            return;

        if (!soundFx)
            return;

        GetAudio().Stop();
        GetAudio().clip = soundFx;
        GetAudio().loop = loop;
        GetAudio().Play();
    }

    private void PlayerFX(AudioClip[] soundFx, bool loop = false)
    {
        if (!behaviour)
            return;

        if (soundFx == null || soundFx.Length == 0)
            return;

        GetAudio().Stop();
        GetAudio().clip = soundFx[Random.Range(0, soundFx.Length)];
        GetAudio().loop = loop;
        GetAudio().Play();
    }


    #region Pause/Continue
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
        GetAudio().Pause();
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
        GetAudio().Play();
    }
    #endregion
}
