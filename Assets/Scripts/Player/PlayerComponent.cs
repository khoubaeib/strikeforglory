﻿using System;
using UnityEngine;

[RequireComponent(typeof(InputComponent))]
[RequireComponent(typeof(MoveComponent))]
public class PlayerComponent : MonoBehaviour
{
    #region Events
    public static Action<PlayerComponent, PlayerComponent> OnAnyPlayerDead;
    public Action OnDead;
    #endregion

    #region Components
    private MoveComponent move;
    private MoveComponent GetMove()
    {
        return move ? move : move = GetComponent<MoveComponent>();
    }

    private InputComponent input;
    private InputComponent GetInput()
    {
        return input ? input : input = GetComponent<InputComponent>();
    }

    private IPlayerComponent[] behaviours;
    private IPlayerComponent[] GetAllBehaviour()
    {
        return behaviours == null ? behaviours = GetComponents<IPlayerComponent>() : behaviours;
    }
    #endregion

    private int id = 0;
    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    private bool isDead = false;
    public bool IsDead
    {
        get { return isDead; }
    }

    private bool behaviour = true;

    public void Die(PlayerComponent killer)
    {
        if (isDead)
            return;
        
        GetInput().Pause();

        isDead = true;

        if (OnDead != null)
            OnDead();
        
        if (OnAnyPlayerDead != null)
            OnAnyPlayerDead(killer, this);
    }
    
    public void DestroyPlayer()
    {
        MatchManager.Instance.DestroyPlayer(id);
    }

    public void Freeze()
    {
        if (!behaviour)
            return;

        GetInput().Pause();
        GetMove().Pause();
    }

    public void Unfreeze()
    {
        if (isDead)
            return;

        if (!behaviour)
            return;

        GetInput().Resume();
        GetMove().Resume();
    }

    #region Pause/Continue
    public void Pause()
    {
        if (GetAllBehaviour() == null)
            return;
        
        foreach (var b in GetAllBehaviour())
            b.Pause();

        behaviour = false;
    }

    public void Resume()
    {
        if (GetAllBehaviour() == null)
            return;

        foreach (var b in GetAllBehaviour())
            b.Resume();

        behaviour = true;
    }
    #endregion
}
