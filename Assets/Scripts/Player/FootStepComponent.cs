﻿using UnityEngine;

[RequireComponent(typeof(PhysicComponent))]
[RequireComponent(typeof(AudioSource))]
public class FootStepComponent : MonoBehaviour, IPlayerComponent
{
    [SerializeField] private FootStepComponentOptions options;

    #region Components
    private AudioSource source;
    private AudioSource GetAudioSource()
    {
        return source ? source : source = GetComponent<AudioSource>();
    }

    private PhysicComponent physic;
    private PhysicComponent GetPhysic()
    {
        return physic ? physic : physic = GetComponent<PhysicComponent>();
    }
    #endregion

    private bool behaviour = true;
    
    public void DoStep()
    {
        if (!behaviour)
            return;

        if (!GetPhysic().IsGrounded || options.IsEmpty)
            return;
        
        var effect = options.GetEffect(GetPhysic().GroundLayer);
        if (!effect)
            return;
        
        GetAudioSource().PlayOneShot(effect);
    }

    #region Pause/Resume
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
        GetAudioSource().Pause();
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
        GetAudioSource().UnPause();
    }
    #endregion
}
