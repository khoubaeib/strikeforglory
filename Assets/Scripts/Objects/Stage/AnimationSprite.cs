﻿using UnityEngine;

public class AnimationSprite : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private float frameSeconds = 1;
    [SerializeField] private bool loop;
    [SerializeField] private bool random;

    private SpriteRenderer spr;
    private int frame = 0;
    private float deltaTime = 0;

    private bool behaviour = true;

    void Start()
    {
        spr = GetComponent<SpriteRenderer>();

        if (random)
            deltaTime -= Random.Range(0f, frameSeconds * 2f);
    }

    void Update()
    {
        if (!behaviour)
            return;

        deltaTime += Time.deltaTime;

        if (deltaTime >= frameSeconds)
        {
            deltaTime -= frameSeconds;
            frame++;
            if (loop)
                frame %= sprites.Length;
            else if (frame >= sprites.Length)
                frame = sprites.Length - 1;
        }
        
        spr.sprite = sprites[frame];
    }

    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }

}