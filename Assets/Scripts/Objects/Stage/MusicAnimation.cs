﻿using UnityEngine;

public class MusicAnimation : MonoBehaviour
{
    [SerializeField] private float deltaTime = 5f;
    
    private GameObject[] childs;
    private float coolDown = 0f;
    
    void Start ()
    {
        childs = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
            childs[i] = transform.GetChild(i).gameObject;
	}
	
	void Update ()
    {
        coolDown -= Time.deltaTime;
        if (coolDown > 0f)
            return;
        
        DisableAllChilds();
        childs[Random.Range(0, childs.Length)].SetActive(true);
        coolDown = deltaTime;
    }

    private void DisableAllChilds()
    {
        foreach (var c in childs)
            c.SetActive(false);
    }
}
