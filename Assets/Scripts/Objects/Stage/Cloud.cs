﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{

    [SerializeField] private float minPos = 0;
    [SerializeField] private float maxPos = 0;
    [SerializeField] private float speed = 0;
    
    private bool behaviour = true;
	
	void Update ()
    {
        if (!behaviour)
            return;

        if (transform.position.x > maxPos)
            SetPositionX(minPos);

        SetPositionX(transform.position.x + (speed * Time.deltaTime));
    }
    
    private void SetPositionX(float x)
    {
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }
}
