﻿using UnityEngine;

public class LightRotation : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float maxAngle = 40;

    private bool behaviour = true;
    private float random = 0;

    void Start()
    {
        random = Random.Range(0, maxAngle * 4);
    }

    void Update()
    {
        if (!behaviour)
            return;

        transform.rotation = Quaternion.AngleAxis(maxAngle - Mathf.PingPong(random + Time.time * speed, maxAngle * 2f), Vector3.forward);
	}

    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }
}
