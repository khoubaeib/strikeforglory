﻿using System;
using UnityEngine;

public class SpeakerDistortion : MonoBehaviour
{

    [SerializeField] private GameObject[] effects;
    [SerializeField] private float coolDown;
    [SerializeField] private float speed;
    [SerializeField] private float from;
    [SerializeField] private float to;

    private bool behaviour = true;
    private float scale = 0;
    private float time = 0;

    void Start()
    {
        scale = from;
    }

    void Update ()
    {
        if (!behaviour)
            return;

        time += Time.deltaTime;
        if (time < coolDown)
            return;

        UpdateScale();
    }
    
    private void UpdateScale()
    {
        if (!behaviour)
            return;

        if (to - scale < 0.1f) {
            Set(false);
            time = 0;
            scale = from;
            transform.localScale = Vector3.one * scale;
        } else {
            Set(true);
            scale = Mathf.MoveTowards(transform.localScale.x, to, speed * Time.deltaTime);
            transform.localScale = Vector3.one * scale;
        }
    }


    private void Set(bool active)
    {
        foreach (var e in effects)
        {
            e.SetActive(active);
        }
    }

    public void Pause()
    {
        if (!behaviour)
            return;
        
        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }
}
