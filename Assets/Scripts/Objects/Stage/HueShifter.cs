﻿using UnityEngine;

public class HueShifter : MonoBehaviour
{
    public float Speed = 1;
    public bool random  =  false;
    private SpriteRenderer rend;

    private bool behaviour = true;
    private float randomValue = 0;
    Color c;

    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        if (random)
            randomValue = Random.Range(0f, 1f);
    }

    void Update()
    {
        if (!behaviour)
            return;

        c = HSBColor.ToColor(new HSBColor(Mathf.PingPong(randomValue + (Time.time * Speed), 1), 1, 1));
        c.a = rend.color.a;
        rend.color = c;

    }
    
    public void Pause()
    {
        if (!behaviour)
            return;

        behaviour = false;
    }

    public void Resume()
    {
        if (behaviour)
            return;

        behaviour = true;
    }

}