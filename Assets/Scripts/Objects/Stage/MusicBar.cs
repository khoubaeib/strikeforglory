﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class MusicBar : MonoBehaviour
{
    [SerializeField] private Sprite[] Bars;
    [SerializeField] private float deltaTime;
    
    private new SpriteRenderer renderer;
    private float coolDown = 0;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    void Update ()
    {
        coolDown -= Time.deltaTime;
        if (coolDown > 0f)
            return;
        
        SetRandomBar();
	}

    private void SetRandomBar()
    {
        Sprite selected;
        do {
            selected = Bars[Random.Range(0, Bars.Length)];
        } while (renderer.sprite == selected);

        renderer.sprite = selected;
        coolDown = deltaTime;
    }
}
