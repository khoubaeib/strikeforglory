﻿using UnityEngine;

public class LevelObjectsManager : Singleton<LevelObjectsManager>
{
    private Cloud[] clouds;
    private LightRotation[] lights;
    private HueShifter[] colorShifters;
    private AnimationSprite[] animations;
    private SpeakerDistortion[] speakers;

    private bool paused = false;

    private void Start()
    {
        animations = FindObjectsOfType<AnimationSprite>();
        colorShifters = FindObjectsOfType<HueShifter>();
        lights = FindObjectsOfType<LightRotation>();
        clouds = FindObjectsOfType<Cloud>();
        speakers = FindObjectsOfType<SpeakerDistortion>();
    }

    public void Pause()
    {
        if (paused)
            return;

        paused = true;
        
        foreach (AnimationSprite a in animations)
            a.Pause();

        foreach (HueShifter cs in colorShifters)
            cs.Pause();

        foreach (LightRotation l in lights)
            l.Pause();

        foreach (Cloud c in clouds)
            c.Pause();

        foreach (SpeakerDistortion s in speakers)
            s.Pause();
    }

    public void Resume()
    {
        if (!paused)
            return;
        
        foreach (AnimationSprite a in animations)
            a.Resume();

        foreach (HueShifter a in colorShifters)
            a.Resume();

        foreach (LightRotation l in lights)
            l.Resume();

        foreach (Cloud c in clouds)
            c.Resume();

        foreach (SpeakerDistortion s in speakers)
            s.Resume();

        paused = false;
    }



}
