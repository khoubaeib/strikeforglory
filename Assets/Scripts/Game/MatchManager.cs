﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MatchMoment
{
    Intro,
    Fight,
    Finish,
}

public class MatchManager : Singleton<MatchManager>
{
    [SerializeField] private SpawnPoint[] spawnPoints;

    private Dictionary<int, PlayerComponent> characters;
    public Dictionary<int , PlayerComponent> Characters
    {
        get { return characters; }
    }
    
    private bool isPaused = false;
    private bool isPausing = false;
    public bool IsPaused
    {
        get { return isPaused; }
    }
    
    private MatchMoment moment = MatchMoment.Intro;

    private void Start ()
    {
        InputHandler.Instance.Category = InputCategory.Default;

        InputHandler.Instance.OnPlayerJoin += OnPlayerJoin;
        InputHandler.Instance.OnPlayerLeave += OnPlayerLeave;

        PlayerComponent.OnAnyPlayerDead += OnAnyPlayerDead;

        SpawnPlayers();
        TakeControl();
        StartRound();
    }

    private void OnDestroy()
    {
        InputHandler.Instance.OnPlayerJoin -= OnPlayerJoin;
        InputHandler.Instance.OnPlayerLeave -= OnPlayerLeave;
        PlayerComponent.OnAnyPlayerDead -= OnAnyPlayerDead;
    }

    #region Players management
    private void OnAnyPlayerDead(PlayerComponent killer, PlayerComponent victim)
    {
        int winner = CheckEndGame();
        if (winner == -1)
            return;
     
        GameData.Instance.RoundsResult.Add(winner);
        FinishRound();
    }

    private void SpawnPlayers()
    {
        characters = new Dictionary<int, PlayerComponent>();
        foreach (var gp in InputHandler.Instance.Gamepads.Values)
            if (gp.IsReady) OnPlayerJoin(gp.Id);
    }

    public void OnPlayerJoin(int key)
    {
        var p = GameData.Instance.GetPlayerCharacter(key);
        var spawn = spawnPoints[(int)Mathf.Repeat(key, spawnPoints.Length - 1f)];
        var c = Instantiate(p, spawn.Position, Quaternion.identity);
        c.GetComponent<MoveComponent>().Move(spawn.FaceRight ? 1 : -1);

        c.Id = key;
        c.gameObject.layer = LayerMask.NameToLayer("Player" + (key + 1));
        CameraManager.Instance.AddTarget(c.transform);
        characters[key] = c;

        if(isPaused)
            c.Pause();
    }

    public void OnPlayerLeave(int key)
    {
        PauseGame();
        DestroyPlayer(key);
    }

    public void DestroyPlayer(int key)
    {
        if (!characters.ContainsKey(key))
            return;

        CameraManager.Instance.RemoveTarget(characters[key].transform);
        Destroy(characters[key].gameObject);
        characters[key] = null;
        characters.Remove(key); 
    }

    public void DestroyAllPlayers()
    {
        if (characters == null || characters.Count == 0)
            return;

        foreach (var c in characters)
        {
            if (!c.Value)
                continue;
            
            int id = c.Key;
            CameraManager.Instance.RemoveTarget(c.Value.transform);
            Destroy(c.Value.gameObject);
        }

        characters.Clear();
    }
    #endregion

    #region Game Control
    public void ReceiveCharacterInput(GamepadWrapper gamepad)
    {
        if (isPaused || moment != MatchMoment.Fight)
            return;

        if (!characters.ContainsKey(gamepad.Id))
            return;
        
        characters[gamepad.Id].GetComponent<InputComponent>().ProcessInput(gamepad);
    }

    private void TakeControl()
    {
        if (characters == null || characters.Count == 0)
            return;

        foreach (var c in characters.Values)
            c.Freeze();
    }

    private void GiveControl()
    {
        if (characters == null || characters.Count == 0)
            return;

        foreach (var c in characters.Values)
            c.Unfreeze();
    }
    #endregion

    #region Start/Finish Match
    public int CheckEndGame()
    {
        List<int> remaining = new List<int>();
        foreach (var c in characters.Values)
        {
            if (c.IsDead)
                continue;

            remaining.Add(c.Id);
        }

        return remaining.Count > 1 ? -1 : remaining[0];
    }

    public void StartRound()
    {
        StartCoroutine(StartRoundInternal());
    }

    private IEnumerator StartRoundInternal()
    {
        moment = MatchMoment.Intro;
        
        MatchUIManager.Instance.SetRoundCount(GameData.Instance.CurrentRound);
        AudioManager.Instance.PlayGameTheme();
        yield return MatchUIManager.Instance.MatchBegin();
        GiveControl();

        moment = MatchMoment.Fight;
    }
    
    public void FinishRound()
    {
        StartCoroutine(FinishRoundInternal());
    }

    private IEnumerator FinishRoundInternal()
    {
        moment = MatchMoment.Finish;
        CameraManager.Instance.Shake();

        TakeControl();

        int current = GameData.Instance.CurrentRound;
        MatchUIManager.Instance.SetPlayerWin(GameData.Instance.RoundsResult[current]);

        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(0.35f);
        Time.timeScale = 1f;

        ScoreBoardScreen.Instance.ResetEntries();
        yield return SlideManager.Instance.SwitchRoutine(ScoreBoardScreen.Slide);
        ScoreBoardScreen.Instance.ResetEntries();
        

        if (GameData.Instance.IsTheLastRound)
            yield return ScoreBoardScreen.Instance.ShowFinalScore();
        else {
            yield return ScoreBoardScreen.Instance.ShowCurrentScore();
            DestroyAllPlayers();
            GameData.Instance.CurrentRound++;
            SpawnPlayers();
            CameraManager.Instance.CenterCamera();
            TakeControl();
        }
    }

    public void ContinueNextRound()
    {
        StartCoroutine(ContinueNextRoundInternal());
    }

    public IEnumerator ContinueNextRoundInternal()
    {
        yield return StartCoroutine(SlideManager.Instance.PopLast.FadeIn());
        yield return StartRoundInternal();
    }
    #endregion

    #region Pause/Resume game
    public void DoPauseResumeCycle()
    {
        if (IsPaused)
            ResumeGame();
        else
            PauseGame();
    }
    
    public void PauseGame()
    {
        if (moment != MatchMoment.Fight)
            return;

        if (isPaused || isPausing)
            return;

        StartCoroutine(PauseGameInternal());
    }

    public void ResumeGame()
    {
        if (moment != MatchMoment.Fight)
            return;

        if (!isPaused || isPausing)
            return;
        
        StartCoroutine(ResumeGameInternal());
    }

    public IEnumerator PauseGameInternal()
    {
        isPausing = true;

        LevelObjectsManager.Instance.Pause();
        AudioManager.Instance.PauseMusic();
        foreach (var c in characters.Values)
            c.Pause();
        
        yield return SlideManager.Instance.SwitchPauseMenu();

        isPaused = true;
        isPausing = false;
    }

    public IEnumerator ResumeGameInternal()
    {
        isPausing = true;
        isPaused = false;
        
        yield return SlideManager.Instance.ExitPauseMenu();

        LevelObjectsManager.Instance.Resume();
        AudioManager.Instance.ResumeMusic();
        foreach (var c in characters.Values)
            c.Resume();

        isPausing = false;
    }
    #endregion
}
