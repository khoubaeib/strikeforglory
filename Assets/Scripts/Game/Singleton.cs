﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    private static T instance = null;
    public static T Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        CheckInstance();
    }

    protected bool CheckInstance()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return false;
        }

        instance = FindObjectOfType<T>();
        return true;
    }
    
}