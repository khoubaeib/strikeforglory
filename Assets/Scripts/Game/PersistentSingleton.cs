﻿using UnityEngine;

public class PersistentSingleton<T> : MonoBehaviour where T : MonoBehaviour
{

    private static T instance = null;
    public static T Instance
    {
        get
        {
            if (instance)
                return instance;

            LevelsManager.LoadPreLoaderScene();
            return instance;
        }
    }

    private void Awake()
    {
        CheckInstance();
    }

    protected bool CheckInstance()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return false;
        }

        instance = FindObjectOfType<T>();
        return true;
    }

}
