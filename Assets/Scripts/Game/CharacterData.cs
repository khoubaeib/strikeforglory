﻿using System;
using UnityEngine;

[Serializable]
public class CharacterData
{
    public PlayerComponent[] prefabs;
    public Sprite[] Thumbnails;
}
