﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum MatchMode
{
    StrikeForGlory,
    StrikeForFun,
    Survival
}

public enum MatchDifficulty
{
    Easy,
    Medium,
    Hard
}


[Serializable]
public class LevelEntry
{
    public string label;
    [SceneName] public string level;
}

public class GameData : PersistentSingleton<GameData>, IRequireSetup
{

    #region Inspector
    [SerializeField] private CharacterData[] characters;
    [SerializeField] private LevelEntry[] levels;
    
    [SerializeField] [SceneName] private string survivalLevel;

    [SerializeField] private int survivalMaxPlayer = 4;
    [SerializeField] private int strikeForGloryMaxPlayer = 2;
    [SerializeField] private int strikeForFunMaxPlayer = 8;
    #endregion

    #region fields
    private int[] selectedCharacters = new int[8];
    private int playersCount = 0;
    private int maxPlayers = 8;

    private int selectedLevel = -1;
    private List<int> roundsResult  = new List<int>();
    private int currentRound = 0;

    private MatchMode mode = MatchMode.StrikeForGlory;
    private MatchDifficulty difficulty = MatchDifficulty.Easy;
    #endregion

    #region Properties
    public CharacterData[] Characters
    {
        get { return characters; }
    }
    
    public int PlayersCount
    {
        get { return playersCount; }
        set
        {
            int oldCount = playersCount;
            playersCount = Mathf.Clamp(value, 0, MaxPlayers);

            int[] oldSelections = new int[oldCount];
            for (int i = 0; i < Mathf.Min(playersCount, oldCount); i++)
                oldSelections[i] = selectedCharacters[i];
            
            selectedCharacters = new int[playersCount];
            for (int i = 0; i < Mathf.Min(playersCount, oldCount); i++)
                selectedCharacters[i] = oldSelections[i];

            for (int i = Mathf.Min(playersCount, oldCount) ; i < playersCount; i++)
                selectedCharacters[i] = 0;
        }
    }

    public int[] SelectedCharacters
    {
        get { return selectedCharacters; }
    }

    public int SelectedLevel
    {
        get { return selectedLevel; }
        set { selectedLevel = value; }
    }

    public bool IsTheLastRound
    {
        get
        {
            int p1 = GetPlayerScore(0);
            int p2 = GetPlayerScore(1);
            
            if (p1 >= 3 && p2 >= 3 && Math.Abs(p1 - p2) < 2)
                return false;

            if (p1 < 4 && p2 < 4)
                return false;
            
            return true;
        }
    }

    public bool IsAdvantageRound
    {
        get
        {
            int p1 = GetPlayerScore(0);
            int p2 = GetPlayerScore(1);

            if (p1 >= 3 && p2 >= 3 && Math.Abs(p1 - p2) < 2)
                return p1 != p2;
            
            return false;
        }
    }

    public string GetPlayer1UIScore()
    {
        int p1 = GetPlayerScore(0);
        int p2 = GetPlayerScore(1);

        if (p1 >= 3 && p2 >= 3 && Math.Abs(p1 - p2) < 2)
            if (p1 == p2)
                return "3";
            else if (p1 > p2)
                return "Adv";
            else
                return "3";

        if (p1 < 4 && p2 < 4)
            return Mathf.Clamp(p1, 0, 3) + "";
        
        if (p1 > p2)
            return "Winner";

        return Mathf.Clamp(p1, 0, 3) + "";
    }

    public string GetPlayer2UIScore()
    {
        int p1 = GetPlayerScore(0);
        int p2 = GetPlayerScore(1);

        if (p1 >= 3 && p2 >= 3 && Math.Abs(p1 - p2) < 2)
            if (p1 == p2)
                return "3";
            else if (p1 > p2)
                return "3";
            else
                return "Adv";

        if (p1 < 4 && p2 < 4)
            return Mathf.Clamp(p2, 0, 3) + "";
        
        if (p1 > p2)
            return Mathf.Clamp(p2, 0, 3) + "";

        return "Winner";
    }
    
    public int GameWinner
    {
        get
        {
            int p1 = GetPlayerScore(0);
            int p2 = GetPlayerScore(1);

            if (p1 >= 3 && p2 >= 3 && Math.Abs(p1 - p2) < 2)
                return -1;

            if (p1 < 4 && p2 < 4)
                return -1;

            if (p1 > p2)
                return 0;

            return 1; 
        }
    }
    
    public List<int> RoundsResult
    {
        get { return roundsResult; }
    }

    public int RoundsCount
    {
        get { return roundsResult.Count; }
    }

    public MatchMode Mode
    {
        get { return mode; }
        set { mode = value; }
    }

    public MatchDifficulty Difficulty
    {
        get { return difficulty; }
        set { difficulty = value; }
    }

    public int MaxPlayers
    {
        get { return maxPlayers; }
        set { maxPlayers = value; }
    }

    public string SurvivalLevel
    {
        get { return survivalLevel; }
    }

    public int CurrentRound
    {
        get { return currentRound; }
        set { currentRound = value; }
    }
    #endregion


    public void SetStrikeForGloryMode()
    {
        mode = MatchMode.StrikeForGlory;
        maxPlayers = strikeForGloryMaxPlayer;
        PlayersCount = Mathf.Min(playersCount, maxPlayers);
    }

    public void SetStrikeForFunMode()
    {
        mode = MatchMode.StrikeForFun;
        maxPlayers = strikeForFunMaxPlayer;
        PlayersCount = Mathf.Min(playersCount, maxPlayers);
    }

    public void SetEasySurvivalMode()
    {
        mode = MatchMode.Survival;
        difficulty = MatchDifficulty.Easy;
        maxPlayers = survivalMaxPlayer;
        PlayersCount = Mathf.Min(playersCount, maxPlayers);
    }

    public void SetMediumSurvivalMode()
    {
        mode = MatchMode.Survival;
        difficulty = MatchDifficulty.Medium;
        maxPlayers = survivalMaxPlayer;
        PlayersCount = Mathf.Min(playersCount, maxPlayers);
    }

    public void SetHardSurvivalMode()
    {
        mode = MatchMode.Survival;
        difficulty = MatchDifficulty.Hard;
        maxPlayers = survivalMaxPlayer;
        PlayersCount = Mathf.Min(playersCount, maxPlayers);
    }
    
    public PlayerComponent GetPlayerCharacter(int character)
    {
        var data = characters[selectedCharacters[character]];
        return data.prefabs[Mathf.Clamp(character, 0, data.prefabs.Length - 1)];
    }


    public Sprite GetPlayerThumbnail(int character)
    {
        var data = characters[selectedCharacters[character]];
        return data.Thumbnails[Mathf.Clamp(character, 0, data.Thumbnails.Length - 1)];
    }

    public string GetSelectedLevel()
    {
        if (selectedLevel == -1)
            return survivalLevel;

        return levels[selectedLevel].level;
    }

    public int GetLevelIndexByName(string name)
    {
        for (int i = 0; i < levels.Length; i++)
            if (levels[i].label.Equals(name))
                return i;
        
        return -1;
    }

    public void Setup()
    {
        InitRoundsResult();

        for (int i = 0; i < SelectedCharacters.Length; i++)
        {
            SelectedCharacters[i] = -1;
        } 
    }

    public void InitRoundsResult()
    {
        roundsResult = new List<int>();
        currentRound = 0;
    }


    public int GetPlayerScore(int index)
    {
        int score = 0;

        foreach(int r in RoundsResult)
            if (r == index)
                score++;

        return score;
    }
}
