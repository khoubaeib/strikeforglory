﻿using System;
using UnityEngine;

public class ManagersSetup : PersistentSingleton<ManagersSetup>
{
    public Action OnInitialized;

    #region Initialization checker
    private static bool isInitialized = false;
    public static bool IsInitialized
    {
        get
        {
            if (!isInitialized)
                return Instance;

            return isInitialized;
        }
    }
    #endregion
    
    void Start()
    {
        if (isInitialized)
            return;

        SetupManagers();

        if(OnInitialized != null)
            OnInitialized();

        isInitialized = true;

        LevelsManager.Instance.CheckPreloaderScene();
    }

    private void SetupManagers()
    {
        IRequireSetup[] setups = GetComponentsInChildren<IRequireSetup>();
        foreach (var m in setups)
            m.Setup();
    }
}
